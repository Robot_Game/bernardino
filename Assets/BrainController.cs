﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainController : MonoBehaviour
{
    private SpiderController sc;
    private bool flag;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9 || collision.gameObject.layer == 10
            && !flag)
        {
            sc.Die();
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        sc = FindObjectOfType<SpiderController>();
    }

    public void TransformBrain()
    {
        flag = true;
        transform.parent = null;
        Destroy(GetComponent<FixedJoint>());
        GetComponent<Rigidbody>().isKinematic = false;
    }
}
