﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour
{
    [SerializeField] private GameObject targetObject;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == targetObject)

            Destroy(gameObject);

    }
}
