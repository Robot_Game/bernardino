﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmController : MonoBehaviour
{
    [Header("Joints")]
    [SerializeField] private float jointsSpring = 20;
    [SerializeField] private float jointsDamper = 2;
    [SerializeField] private float jointsWeight = 0.01f;
    [Header("Hands")]
    [SerializeField] private float handSpring = 20;
    [SerializeField] private float handDamper = 2;
    [SerializeField] private float handWeight = 0.06f;

    //[SerializeField] private Rigidbody shoulder;
    //private HingeJoint hand;
    //private HingeJoint[] joints;
    private Rigidbody hand;
    private Rigidbody[] joints;
    private Vector3[] initPositions;

    private JointController[] jointControllers;

    private int jointsCount;

    private void Start()
    {
        GetObjects();
        GetInitialPositions();
        AddSpringJoints();
        DestroyHingeJoints();
    }

    private void GetObjects()
    {
        /* Hinge Joints method*/
        /*hand = transform.GetChild(transform.childCount - 1).GetComponent<HingeJoint>();

        joints = new HingeJoint[transform.childCount];
        for (int i = 0; i < transform.childCount - 1; i++)
        {
            joints[i] = transform.GetChild(i).GetComponent<HingeJoint>();
        }*/
        jointsCount = transform.childCount;

        initPositions = new Vector3[jointsCount];
        /* Swap method*/
        jointControllers = new JointController[jointsCount];

        hand = transform.GetChild(jointsCount - 1).GetComponent<Rigidbody>();
        jointControllers[0] = hand.GetComponent<JointController>();
        joints = new Rigidbody[jointsCount];
        for (int i = 0; i < jointsCount - 1; i++)
        {
            joints[i] = transform.GetChild(i).GetComponent<Rigidbody>();
            jointControllers[i + 1] = joints[i].GetComponent<JointController>();
        }
    }

    private void GetInitialPositions()
    {
        // Hand
        initPositions[0] = transform.GetChild(jointsCount - 1).localPosition;
        // Joints
        for (int i = 0; i < jointsCount - 1; i++)

            initPositions[i + 1] = transform.GetChild(i).transform.localPosition;

    }

    public void ResetInitialPositions()
    {
        //Vector3 newPos = new Vector3(initPositions[0].x, initPositions[0].y, initPositions[0].z);
        hand.transform.localPosition.Set(initPositions[0].x, initPositions[0].y, initPositions[0].z);

        for (int i = 0; i < jointsCount - 1; i++)
        {
            //newPos = new Vector3(initPositions[i + 1].x, initPositions[i + 1].y, initPositions[i + 1].z);
            joints[i].transform.localPosition.Set(initPositions[i + 1].x, initPositions[i + 1].y, initPositions[i + 1].z);
        }

    }

    // Swap Joint method
    public void AddSpringJoints(/*ref bool flag*/)
    {
        HingeJoint handHinge = null;

        if (hand.GetComponent<HingeJoint>() == null)
            handHinge = hand.gameObject.AddComponent<HingeJoint>();
        else
            handHinge = hand.GetComponent<HingeJoint>();

        hand.constraints = RigidbodyConstraints.None;
        hand.mass = handWeight;

        JointSpring handHingeSpring = handHinge.spring;
        handHingeSpring.spring = handSpring;
        handHingeSpring.damper = handDamper;
        handHinge.spring = handHingeSpring;
        handHinge.useSpring = true;
        handHinge.anchor = new Vector3(0.02f, 0, 0);
        handHinge.autoConfigureConnectedAnchor = false;
        handHinge.connectedAnchor = Vector3.zero;
        handHinge.connectedBody = jointControllers[0].connectedBody.GetComponent<Rigidbody>();


        DisableCustomSpringBehaviour(0);

        for (int i = 0; i < jointsCount - 1; i++)
        {
            HingeJoint jointHinge = joints[i].gameObject.AddComponent<HingeJoint>();
            JointSpring hingeSpring = jointHinge.spring;
            hingeSpring.spring = jointsSpring;
            hingeSpring.damper = jointsDamper;
            jointHinge.spring = hingeSpring;
            jointHinge.useSpring = true;
            jointHinge.autoConfigureConnectedAnchor = false;
            jointHinge.anchor = new Vector3(0.02f, 0, 0);
            jointHinge.connectedAnchor = Vector3.zero;
            jointHinge.connectedBody = jointControllers[i + 1].connectedBody.GetComponent<Rigidbody>();
            joints[i].mass = jointsWeight;

            DisableCustomSpringBehaviour(i + 1);
        }

        //flag = true;
    }

    // Swap Joint method
    public void ActivateSprings(bool value)
    {
        HingeJoint handHinge = null;

        handHinge = hand.GetComponent<HingeJoint>();

        handHinge.useSpring = value;

        for (int i = 0; i < jointsCount - 1; i++)
        {
            HingeJoint jointHinge = joints[i].gameObject.GetComponent<HingeJoint>();
            jointHinge.useSpring = value;
        }

        //flag = true;
    }

    public void AddHingeJoints(/*ref bool flag*/)
    {
        HingeJoint handHinge = null;

        if (hand.GetComponent<HingeJoint>() == null)
            handHinge = hand.gameObject.AddComponent<HingeJoint>();
        else
            handHinge = hand.GetComponent<HingeJoint>();

        if (hand.GetComponent<FixedJoint>() != null)
            Destroy(hand.GetComponent<FixedJoint>());
        hand.GetComponent<Rigidbody>().isKinematic = true;
        hand.GetComponent<Rigidbody>().mass = 1;
        handHinge.connectedBody = jointControllers[0].connectedBody.GetComponent<Rigidbody>();

        DisableCustomSpringBehaviour(0);

        for (int i = 0; i < jointsCount - 1; i++)
        {
            HingeJoint jointHinge = joints[i].gameObject.AddComponent<HingeJoint>();
            jointHinge.GetComponent<Rigidbody>().mass = 1;
            jointHinge.connectedBody = jointControllers[i + 1].connectedBody.GetComponent<Rigidbody>();

            DisableCustomSpringBehaviour(i + 1);
        }

        //flag = true;
    }

    public void ChangeJointsWeight(float weight)
    {
        if (weight == 0) weight = handWeight;

        HingeJoint handHinge = hand.GetComponent<HingeJoint>();

        handHinge.GetComponent<Rigidbody>().mass = weight;

        if (weight == handWeight) weight = jointsWeight;
        for (int i = 0; i < jointsCount - 1; i++)
        {
            HingeJoint jointHinge = joints[i].GetComponent<HingeJoint>();
            jointHinge.GetComponent<Rigidbody>().mass = weight;
        }
    }

    public void SetJointsHeight(float height)
    {
        hand.position = new Vector3(hand.position.x, height, hand.position.z);

        for (int i = 0; i < jointsCount - 1; i++)

            joints[i].position = new Vector3(joints[i].position.x, height, joints[i].position.z);

    }

    public void ChangeGravity(bool value)
    {
        hand.useGravity = value;

        for (int i = 0; i < jointsCount - 1; i++)

            joints[i].useGravity = value;

    }

    public void DestroyHingeJoints()
    {
        EnableCustomSpringBehaviour(0);

        hand.constraints = RigidbodyConstraints.FreezeRotation;

        HingeJoint[] hinges = hand.GetComponents<HingeJoint>();
        if (hinges.Length == 1) Destroy(hinges[0]);
        if (hinges.Length == 2)
        {
            Destroy(hinges[1]);
            Destroy(hinges[0]);
        }

        for (int i = 0; i < jointsCount - 1; i++)
        {
            EnableCustomSpringBehaviour(i + 1);
            Destroy(joints[i].GetComponent<HingeJoint>());
        }

    }

    private void DisableCustomSpringBehaviour(int i)
    {
        //jointControllers[i].SetIsGrabbingValue(true);
        jointControllers[i].enabled = false;
    }

    private void EnableCustomSpringBehaviour(int i)
    {
        //jointControllers[i].SetIsGrabbingValue(false);
        jointControllers[i].enabled = true;
    }

    public bool MaxDistanceReached()
    {

        for (int i = 0; i < jointsCount; i++)
        {
            if (jointControllers[i].MaxDistanceReached()) return true;
        }

        return false;
    }
}
