﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Camera CurrentCam { get; private set; } 

    [SerializeField] private Transform target;
    [SerializeField] private Vector3 offSet;
    [SerializeField] private float followPlayerSpeed = 0.13f;
    [SerializeField] private float switchSpeed = 0.13f;

    private bool isMainCamera = true;
    private bool isFollowing = true;
    private bool isSwitching = false;

    private void Awake()
    {
        CurrentCam = GetComponent<Camera>();
    }

    private void Start()
    {
        if(target == null) target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isFollowing) FollowPlayer();
    }

    private void Update()
    {
        if (isSwitching && !isFollowing)
            ActivateCameraSwitch(CurrentCam.transform, isMainCamera);
    }

    private void FollowPlayer()
    {
        Vector3 desiredPosition = target.position + offSet;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position,
            desiredPosition, followPlayerSpeed);
        transform.position = smoothedPosition;
    }

    public void ActivateCameraSwitch(Transform newCamera, bool isMainCamera)
    {
        Debug.Log("Calling cam controller");
        this.isMainCamera = isMainCamera;
        CurrentCam = newCamera.GetComponent<Camera>();
        isSwitching = true;

        if (isMainCamera && !isFollowing)
        {
            if (ChangeCamera(newCamera))
            {
                isSwitching = false;
                isFollowing = true;
            }
        }
        if (!isMainCamera && isSwitching)
        {
            if (isFollowing) isFollowing = false;
            if (ChangeCamera(newCamera))
            {
                isSwitching = false;
            }
        }
    }

    private bool ChangeCamera(Transform newCamera)
    {
        if ((transform.position - newCamera.position).magnitude > 0.05f) MoveCamera(newCamera);
        if (Quaternion.Angle(transform.rotation, newCamera.rotation) > 0.05f) RotateCamera(newCamera);

        if ((transform.position - newCamera.position).magnitude < 0.05f
            && Quaternion.Angle(transform.rotation, newCamera.rotation) < 0.05f)
        {
            return true;
        }

        return false;
    }

    private void MoveCamera(Transform newCamera)
    {
        transform.position = Vector3.Lerp(transform.position, newCamera.position, switchSpeed * Time.deltaTime);
    }
    private void RotateCamera(Transform newCamera)
    {
        Vector3 newAngle = new Vector3(
            Mathf.LerpAngle(transform.rotation.eulerAngles.x, newCamera.rotation.eulerAngles.x, switchSpeed * Time.deltaTime),
            Mathf.LerpAngle(transform.rotation.eulerAngles.y, newCamera.rotation.eulerAngles.y, switchSpeed * Time.deltaTime),
            Mathf.LerpAngle(transform.rotation.eulerAngles.z, newCamera.rotation.eulerAngles.z, switchSpeed * Time.deltaTime));

        transform.eulerAngles = newAngle;
    }
}
