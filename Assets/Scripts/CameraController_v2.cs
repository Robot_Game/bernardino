﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController_v2 : MonoBehaviour
{

    [SerializeField] private GameObject CameraFollowObj;
    [SerializeField] private Transform cam;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float cameraMoveSpeed = 100f;
    [SerializeField] private float minClampAngle = -30f;
    [SerializeField] private float maxClampAngle = 60f;
    [SerializeField] private float inputSensivity = 30f;
    [SerializeField] private float collisionSmoothness = 10f;
    [SerializeField] private float minZoomDistance = 3f;
    [SerializeField] private float minDistance = 1f;
    [SerializeField] private float maxDistance;
    [SerializeField] private bool setMaxDistance;
    private float mouseX = 0f;
    private float mouseY = 0f;
    private float rotX = 0f;
    private float rotY = 0f;
    private float zoom;
    private float dollyDistance;
    private Vector3 dollyDirection;

    // Start is called before the first frame update
    void Awake()
    {
        SetInitialParameters();
    }

    // Update is called once per frame
    void Update()
    {
        FollowPlayer();
        MoveOnCollision();

        if (Input.GetMouseButton(2))
        {
            ReadMouseInput();
            RotateCamera();
        }

        ManageZoom();

    }

    private void SetInitialParameters()
    {
        Vector3 rot = transform.localRotation.eulerAngles;
        rotX = rot.x;
        rotY = rot.y;

        if (!setMaxDistance)
            maxDistance = Vector3.Distance(transform.position, cam.position);
        dollyDirection = cam.localPosition.normalized;
        dollyDistance = cam.localPosition.magnitude;
        zoom = maxDistance*3/4;

    }

    private void ReadMouseInput()
    {
        mouseX = Input.GetAxis("M_Mouse X");
        mouseY = -Input.GetAxis("M_Mouse Y");

        rotX += mouseY * inputSensivity * Time.deltaTime;
        rotY += mouseX * inputSensivity * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, minClampAngle, maxClampAngle);
    }

    private void ManageZoom()
    {
        float zoomChangeAmount = 30f;

        if (Input.GetAxis("Mouse ScrollWheel") > 0f) zoom -= zoomChangeAmount * Time.deltaTime;
        if (Input.GetAxis("Mouse ScrollWheel") < 0f) zoom += zoomChangeAmount * Time.deltaTime;

        zoom = Mathf.Clamp(zoom, minZoomDistance, maxDistance);
    }

    private void RotateCamera()
    {
        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0);
        transform.localRotation = localRotation;
    }

    private void FollowPlayer()
    {
        Transform target = CameraFollowObj.transform;

        float step = cameraMoveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }

    private void MoveOnCollision()
    {
        Vector3 desiredPos = transform.TransformPoint(dollyDirection * maxDistance);
        RaycastHit hit;

        if (Physics.Linecast(transform.position, desiredPos, out hit, layerMask))
        {
            dollyDistance = Mathf.Clamp((hit.distance * 0.8f), minDistance, maxDistance);
        }
        else dollyDistance = zoom;

        cam.localPosition = Vector3.Lerp(
            cam.localPosition,
            dollyDirection * dollyDistance,
            Time.deltaTime * collisionSmoothness);
    }
}
