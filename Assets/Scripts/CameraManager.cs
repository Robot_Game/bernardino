﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CameraManager
{

    public static Camera CurrentCamera { get; private set; }
    public static bool IsOnMainCam { get; private set; }

    private static Camera mainCam;

    public static Camera ActivateSpecificCam(Camera cam, bool value)
    {
        cam.enabled = value;
        mainCam.enabled = !value;

        return GetActiveCamera();
    }

    private static Camera GetActiveCamera()
    {
        Camera[] cams = Object.FindObjectsOfType<Camera>();
        foreach(Camera cam in cams)
        {
            if (cam.isActiveAndEnabled) return cam;
        }

        Debug.Log("No cam could be found");

        return mainCam;
    }
}
