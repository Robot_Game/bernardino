﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerasManager : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Camera topCamera;

    private bool currentCamera;

    private int flag;

    // Start is called before the first frame update
    void Start()
    {
        currentCamera = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab)) SwitchCameras();
    }

    private void SwitchCameras()
    {
        Debug.Log("Tab");
        bool flag = false;

        if (currentCamera == true)
        {
            Debug.Log("Should Switch");
            topCamera.enabled = true;
            mainCamera.enabled = false;
            currentCamera = false;
            flag = true;
        }
        if( currentCamera == false && !flag)
        {
            topCamera.enabled = false;
            mainCamera.enabled = true;
            currentCamera = true;
        }
    }
}
