﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheats : MonoBehaviour
{
    [SerializeField] private Transform[] checkpoints;

    private SaveController sc;
    private Transform currentCheckpoint;
    private Transform player;

    // Start is called before the first frame update
    void Start()
    {
        sc = FindObjectOfType<SaveController>();
        currentCheckpoint = checkpoints[0];

        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            for (int i = 0; i < checkpoints.Length; i++)
            {
                if (checkpoints[i] == sc.currentCheckpoint)
                {

                    if (checkpoints[i] == checkpoints[checkpoints.Length - 1])
                    {
                        ResetCheckpoints();
                        sc.SetCheckpoint(checkpoints[0]);
                        currentCheckpoint = checkpoints[0];
                    }
                    else
                    {
                        sc.SetCheckpoint(checkpoints[i + 1]);
                        currentCheckpoint = checkpoints[i + 1];
                    }
                    break;
                }
            }

            if (currentCheckpoint != null) Teleport(currentCheckpoint);
        }

        currentCheckpoint = sc.currentCheckpoint;
    }

    private void Teleport(Transform target)
    {
        player.position = target.position;
    }

    private void ResetCheckpoints()
    {
        foreach (Transform t in checkpoints)
        {
            t.GetComponent<Checkpoint>().ResetActivatedBool();
        }

    }
}
