﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{

    public bool activated { get; private set; } = false;

    private SaveController saveController;
    private PauseMenu pauseMenu;
    private PlayerController pc;

    private void Awake()
    {
        pc = FindObjectOfType<PlayerController>();
        saveController = FindObjectOfType<SaveController>();
        pauseMenu = FindObjectOfType<PauseMenu>();
        if(name == "Checkpoint 0")
        {
            transform.position = pc.transform.position;
            transform.rotation = pc.transform.rotation;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if(other.tag == "Player" && !activated && !name.Contains("Endgame"))
        {
            saveController.SetCheckpoint(transform);
            activated = true;
        }
        if (other.tag == "Player" && !activated && name.Contains("Endgame"))
        {
            pauseMenu.Quit();
        }
    }

    public void ResetActivatedBool()
    {
        activated = false;
    }
}
