﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{

    [SerializeField] private Transform target;
    [SerializeField] private bool[] ignoreXYZ = new bool[3];
    [SerializeField] private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        if(offset == Vector3.zero)
        offset = transform.localPosition;
        if (ignoreXYZ.Length != 3) ignoreXYZ = new bool[3];
    }

    // Update is called once per frame
    void Update()
    {
        Follow();
    }

    private void Follow()
    {

        transform.position = GetPosition();
    }

    private Vector3 GetPosition()
    {
        float x;
        float y;
        float z;

        if (ignoreXYZ[0]) x = transform.position.x + offset.x;
        else x = target.position.x + offset.x;

        if (ignoreXYZ[1]) y = transform.position.y;
        else y = target.position.y + offset.y;

        if (ignoreXYZ[2]) z = transform.position.z + offset.z;
        else z = target.position.z + offset.z;

        return new Vector3(x, y, z);
    }
}
