﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowRotation : MonoBehaviour
{
    [SerializeField] private Transform target;

    [SerializeField] private bool[] ignoreXYZ = new bool[3];

    // Update is called once per frame
    void Update()
    {
        Rotate();
    }

    private void Rotate()
    {
        float xRot;
        float yRot;
        float zRot;

        if (ignoreXYZ[0]) xRot = transform.eulerAngles.x;
        else xRot = target.eulerAngles.x;
        if (ignoreXYZ[1]) yRot = transform.eulerAngles.y;
        else yRot = target.eulerAngles.y;
        if (ignoreXYZ[2]) zRot = transform.eulerAngles.z;
        else zRot = target.eulerAngles.x;

        transform.rotation = Quaternion.Euler(xRot, yRot, zRot);
    }
}
