﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabController : MonoBehaviour
{
    public Transform MiddleObject => middleObject;
    public Transform CurrentGrabbedPivot { get; private set; }
    public Vector3 MiddlePos { get; private set; }
    public float AverageDistance { get; private set; }
    public float GrabbedHandDistance { get; private set; }

    public bool[] Grabbed { get; private set; } = new bool[2];
    public bool[] Picked { get; private set; } = new bool[2];

    public float MaxGrabDistance => maxGrabDistance;
    public float OffsetDistance => offsetDistance;

    public Transform[] hand = new Transform[2];

    [SerializeField] private Transform middleObject;
    [SerializeField] private Transform[] pickedObjectHolder = new Transform[2];
    [SerializeField] private float throwDistance = 20;
    [SerializeField] private float throwSpeed = 5; //last time the value was 100
    [SerializeField] private float throwTime = 0.5f; // last time the value was 0.3
    [SerializeField] private float maxGrabDistance = 24f;
    [SerializeField] private float offsetDistance = 2.1f;
    [SerializeField] private LayerMask layerMask;

    private Transform currentPickedObject;
    private Transform[] newGrabbedObject = new Transform[2];
    private Transform[] oldGrabbedObject = new Transform[2];
    private Rigidbody[] handConnectedBody = new Rigidbody[2];
    private Vector3[] handPosition = new Vector3[2];
    private bool[] handInput = new bool[2];
    private bool[] swinging = new bool[2];
    private bool[] throwHand = new bool[2];
    private bool[] throwFlag = new bool[2];
    private bool[] hitTarget = new bool[2];
    private float[] throwTimer = new float[2];
    private string[] interactionType = new string[2];

    private Pickable oldPickable;
    private bool hasPickable;
    private Coroutine pickCoroutine;

    // Distances
    private float[] handDistance = new float[2];
    private ArmController[] armControllers = new ArmController[2];

    // Scripts
    private PlayerController pc;
    private UIManager uiManager;

    // Start is called before the first frame update
    void Start()
    {
        // Get the controller for each arm
        armControllers[0] = transform.parent.Find("LeftArm").GetComponent<ArmController>();
        armControllers[1] = transform.parent.Find("RightArm").GetComponent<ArmController>();
        CurrentGrabbedPivot = middleObject;
        pc = GetComponent<PlayerController>();

        uiManager = FindObjectOfType<UIManager>();
    }

    private void Update()
    {
        GetGrabbedHand();
        SetMiddleObjectPos();
        ManageHandInputs();
        ManagePickables();
        CheckForTargetHit(0);
        CheckForTargetHit(1);

    }

    private void CheckForTargetHit(int i)
    {
        if (HandHitTarget(i))
        {
            hitTarget[i] = true;
        }
        else hitTarget[i] = false;
    }

    private void ManagePickables()
    {
        if (Input.GetKeyDown(KeyCode.Q)
            && hasPickable
            && oldPickable != null) DropObject();
    }

    private void SetMiddleObjectPos()
    {
        middleObject.position = MiddlePos;
    }

    public void ManageDistance()
    {
        MiddlePos = GetMiddlePos();
        SetHandsDistance();
        GetAverageDistance();
        GetGrabbedHandDistance();
    }

    private Vector3 GetMiddlePos() => ((hand[1].position + hand[0].position) * 0.5f);

    private void GetGrabbedHand()
    {
        if (Grabbed[0] && !Grabbed[1]) CurrentGrabbedPivot = hand[0];
        else if (!Grabbed[0] && Grabbed[1]) CurrentGrabbedPivot = hand[1];
        if (Grabbed[0] && Grabbed[1])
        {
            //if (!pc.IsGrounded)
            //{
            if (hand[0].position.y > hand[1].position.y + 3) CurrentGrabbedPivot = hand[0];
            else if (hand[1].position.y > hand[0].position.y + 3) CurrentGrabbedPivot = hand[1];
            else CurrentGrabbedPivot = middleObject;
            //}
            //else CurrentGrabbedPivot = middleObject;
        }
    }

    private void SetHandsDistance()
    {
        if (Grabbed[0]) handDistance[0] = GetHandDistance(0);
        else handDistance[0] = 0;
        if (Grabbed[1]) handDistance[1] = GetHandDistance(1);
        else handDistance[1] = 0;
    }

    private float GetHandDistance(int i) => GetDistance(hand[i].position, transform.position);

    private void GetAverageDistance()
    {
        if (Grabbed[1] || Grabbed[0]) AverageDistance = GetDistance(MiddlePos, transform.position);
        else /*averageDistance = offsetDistance - 0.1f;*/ AverageDistance =
                GetDistance(MiddlePos, transform.position);
    }

    private void GetGrabbedHandDistance()
    {
        if (Grabbed[0] && !Grabbed[1]) GrabbedHandDistance = GetDistance(hand[0].position, transform.position);
        if (!Grabbed[0] && Grabbed[1]) GrabbedHandDistance = GetDistance(hand[1].position, transform.position);
        if (Grabbed[0] && Grabbed[1]) GrabbedHandDistance = GetDistance(MiddlePos, transform.position);
    }
    private float GetDistance(Vector3 pos1, Vector3 pos2) => Vector3.Distance(pos1, pos2);


    public bool IsMovingToHand(Vector3 moveDirection, Transform player, float angleOffset)
    {
        //Debug.Log("calling");

        Vector3 handPos = Vector3.zero;
        if (Grabbed[0]) handPos = hand[0].position;
        else if (Grabbed[1]) handPos = hand[1].position;
        Vector3 lookDir = handPos - player.position;
        //Debug.Log(Vector3.Angle(moveDirection, handPos - player.position));
        if (Mathf.Abs(Vector3.Angle(
            new Vector3(moveDirection.x, 0, moveDirection.z),
            new Vector3(lookDir.x, 0, lookDir.z))
            ) < angleOffset)
        {
            return true;
        }
        return false;
    }

    private void ManageHandInputs()
    {
        if (HandInputDown(0)) handInput[0] = true;
        if (HandInputUp(0)) handInput[0] = false;

        if (HandInputDown(1)) handInput[1] = true;
        if (HandInputUp(1)) handInput[1] = false;
    }

    private bool HandInputDown(int i) => Input.GetMouseButtonDown(i);
    private bool HandInputUp(int i) => Input.GetMouseButtonUp(i);
    private bool HandInput(int i) => Input.GetMouseButton(i);

    public void ReadGrabInput()
    {
        ManageFirstHandClick();
        ManageSecondHandClick();

    }

    public void ManageGrab()
    {
        ManageHandThrow();
        ManageLockState();
    }

    private void ManageFirstHandClick()
    {
        //If there's nothing grabbed
        if (handInput[0] && !throwHand[0])
        {
            //Set interaction type (it's set to null if the item is not an interactible)
            interactionType[0] = CheckForInteractible(0);
            // Reset grabbable object and check if there
            if (interactionType[0] != null)
            {
                throwHand[0] = true;
            }
            //if(throwHand[0]) newGrabbedObject[0] = null;
        }
        if (handInput[1] && !throwHand[1])
        {
            interactionType[1] = CheckForInteractible(1);
            // Reset grabbable object and check if there
            if (interactionType[1] != null)
            {
                throwHand[1] = true;
            }
            //if (throwHand[1]) newGrabbedObject[1] = null;
        }
    }

    private void ManageHandThrow()
    {
        if (throwHand[0] && !Grabbed[0] && throwTimer[0] < throwTime
            && handInput[0])
        {
            throwTimer[0] += Time.deltaTime;
            ThrowHand(0);
        }
        if (throwHand[1] && !Grabbed[1] && throwTimer[1] < throwTime
            && handInput[1])
        {
            throwTimer[1] += Time.deltaTime;
            ThrowHand(1);
        }

    }

    private void ManageSecondHandClick()
    {
        // If it's grabbed already or it didnt grab after 1 second
        if (!handInput[0] && (Grabbed[0] || Picked[0])) UnlockHand(0);

        if (!handInput[1] && (Grabbed[1] || Picked[1])) UnlockHand(1);

        // Unlock with Jump
        /*if (!pc.IsGrounded && InputManager.GetJumpButtonDown())
        {
            if (Grabbed[0]) UnlockHand(0);
            if (Grabbed[1]) UnlockHand(1);
        }*/

    }

    private void ManageLockState()
    {
        //LeftHand
        if ((((throwTimer[0] > throwTime && !Grabbed[0] && !Picked[0] && throwHand[0])
            || handDistance[0] > maxGrabDistance + 10) && !swinging[0])
            || ((Grabbed[0] || throwHand[0]) && !handInput[0])) { Debug.Log("Não está a dar :/"); UnlockHand(0); }
        //RightHand
        if (((throwTimer[1] > throwTime && !Grabbed[1] && !Picked[1] && throwHand[1])
            || handDistance[1] > maxGrabDistance + 10) && !swinging[1] 
            || ((Grabbed[1] || throwHand[1]) && !handInput[1])) UnlockHand(1);
    }


    private string CheckForInteractible(int i)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Physics.Raycast(ray, out hit, 200, layerMask);
        if (Vector3.Distance(hit.point, transform.position) <= throwDistance)
        {
            if (hit.collider != null && hit.collider.GetComponent<Grabbable>() != null)
                return CheckForGrabbable(i, hit);
            if (hit.collider != null && hit.collider.GetComponent<Pickable>() != null)
                return CheckForPickable(i, hit);
        }
        else if (hit.collider != null)
        {
            if (hit.collider.GetType() == typeof(MeshCollider))
            {
                MeshCollider mesh = (MeshCollider)hit.collider;
                if (mesh.convex)
                {

                    Vector3 closestPoint =
                        //GetComponent<Collider>().ClosestPoint(hit.collider.transform.position);
                        Physics.ClosestPoint(transform.position, hit.collider,
                                    hit.collider.transform.position,
                                    hit.collider.transform.rotation);
                    float closestPointDistance =
                        Vector3.Distance(transform.position, closestPoint);

                    if (closestPointDistance <= throwDistance)
                    {
                        if (hit.collider != null && hit.collider.gameObject.GetComponent<Grabbable>() != null)
                            return CheckForGrabbable(i, hit, closestPoint);

                    }
                }
            }
        }
        newGrabbedObject[i] = null;

        return null;
    }

    private string CheckForGrabbable(int i, RaycastHit hit)
    {
        handPosition[i] = hit.point;
        newGrabbedObject[i] = hit.transform;
        return "Grabbable";
    }

    private string CheckForGrabbable(int i, RaycastHit hit, Vector3 pos)
    {
        handPosition[i] = pos;
        newGrabbedObject[i] = hit.transform;
        return "Grabbable";
    }

    private string CheckForPickable(int i, RaycastHit hit)
    {
        Pickable pickable = hit.transform.GetComponent<Pickable>();
        pickable.SetPickedBool(true);
        handPosition[i] = hit.transform.position;
        newGrabbedObject[i] = pickable.transform;
        return "Pickable";
    }

    private void ThrowHand(int i)
    {

        if (!throwFlag[i])
            AddHinges(i);

        Rigidbody rb = hand[i].GetComponent<Rigidbody>();
        if (hitTarget[i])
        {
            Debug.Log("Hand reached target!");
            if (IsGrabbable(i) && handInput[i])
            {
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
                Grab(i);
            }

            if (IsPickable(i)) Pick(i);
        }
        else if (throwTimer[i] < throwTime)
        {
            if (IsGrabbable(i)) ChangeHandPosition(i, throwSpeed, rb);

            if (IsPickable(i)) ChangeHandPosition(i, throwSpeed, rb);
        }
    }

    private void ChangeHandPosition(int i, float speed, Rigidbody rb)
    {
        float step = speed * Time.deltaTime;
        hand[i].position = Vector3.MoveTowards(hand[i].position, handPosition[i], step);
        //rb.MovePosition(handPosition[i]);
        //rb.AddForce(Vector3.ClampMagnitude((handPosition[i] - hand[i].position) * throwSpeed, throwSpeed), ForceMode.Force);
    }

    private bool HandHitTarget(int i) => Vector3.Distance(hand[i].position, handPosition[i]) < 0.4f;

    private bool IsGrabbable(int i) => interactionType[i] == "Grabbable";

    private bool IsPickable(int i) => interactionType[i] == "Pickable";

    private void AddHinges(int i)
    {
        armControllers[i].AddSpringJoints();
        throwFlag[i] = true;
    }

    public void Grab(int i)
    {
        hand[i].position = handPosition[i];

        SetConnectedBody("Grab", i);

        //HingeJoint grabHinge = leftHand.gameObject.AddComponent<HingeJoint>();
        //grabHinge.connectedBody = leftConnectedBody;

        LockHand("Grab", i);

        //rb.constraints = RigidbodyConstraints.FreezeAll;
        oldGrabbedObject[i] = newGrabbedObject[i];
        throwHand[i] = false;
        Grabbed[i] = true;
    }

    private void Pick(int i)
    {
        SetConnectedBody("Pick", i);

        LockHand("Pick", i);

        oldGrabbedObject[i] = newGrabbedObject[i];
        throwHand[i] = false;
        Picked[i] = true;
        pickCoroutine = StartCoroutine(PickObject(i));
    }

    private void SetConnectedBody(string interactionType, int i)
    {
        if (newGrabbedObject[i].GetComponent<Rigidbody>() == null) AddRigidbody(interactionType, i);
        else handConnectedBody[i] = newGrabbedObject[i].gameObject.GetComponent<Rigidbody>();
    }

    private void AddRigidbody(string interactionType, int i)
    {
        if (interactionType == "Grab")
        {
            handConnectedBody[i] = newGrabbedObject[i].gameObject.AddComponent<Rigidbody>();
            handConnectedBody[i].isKinematic = true;
        }
        if (interactionType == "Pick")
        {
            handConnectedBody[i] = newGrabbedObject[i].gameObject.AddComponent<Rigidbody>();
            handConnectedBody[i].mass = 0.001f;
        }
    }

    private void LockHand(string interactionType, int i)
    {

        if (interactionType == "Grab")
        {
            FixedJoint grabHinge = hand[i].gameObject.GetComponent<FixedJoint>();
            if (grabHinge == null) grabHinge = hand[i].gameObject.AddComponent<FixedJoint>();

            grabHinge.connectedBody = handConnectedBody[i];
            // Verificar quando dá break e chamar o UnlockHand() qnd isso acontece
            //grabHinge.breakForce = 1500;
            //grabHinge.breakTorque = 1500;
            if (handConnectedBody[i].GetComponent<Grabbable>().ChangeKinematic)
                handConnectedBody[i].GetComponent<Grabbable>().ChangeKinematicValue();

            uiManager.EnableGrabFeedback(i);
        }
        if (interactionType == "Pick")
        {
            FixedJoint grabHinge = hand[i].gameObject.GetComponent<FixedJoint>();
            if (grabHinge == null) grabHinge = hand[i].gameObject.AddComponent<FixedJoint>();

            grabHinge.connectedBody = handConnectedBody[i];
            // Verificar quando dá break e chamar o UnlockHand() qnd isso acontece
            //grabHinge.breakForce = 1500;
            //grabHinge.breakTorque = 1500;
            if (handConnectedBody[i].GetComponent<Pickable>().ChangeKinematic)
                handConnectedBody[i].GetComponent<Pickable>().ChangeKinematicValue();

            handConnectedBody[i].GetComponent<Pickable>().ChangeLayer(true);
        }
    }

    private IEnumerator PickObject(int i)
    {
        bool flag = false;
        LayerMask initHandLayer = hand[i].gameObject.layer;
        Pickable pickable = handConnectedBody[i].GetComponent<Pickable>();
        pickable.GetComponent<Rigidbody>().useGravity = false;
        if (hasPickable || oldPickable != null) DropObject();
        hand[i].gameObject.layer = 18;
        while (Vector3.Distance(pickable.Parent.position, pickable.Target.position) > 0.1f)
        {
            if (!flag && Vector3.Distance(pickable.Parent.position, pickable.Target.position) < 0.4f)
            {
                UnlockHand(i);
                flag = true;
            }
            pickable.MoveToTarget(6);
            yield return null;
        }
        hand[i].gameObject.layer = 18;
        pickable.Lock();
        oldPickable = pickable;
        hasPickable = true;
        StopCoroutine(pickCoroutine);
    }

    private void DropObject()
    {
        oldPickable.GetComponent<Rigidbody>().useGravity = true;
        oldPickable.ChangeLayer(false);
        oldPickable.Unlock();
        oldPickable = null;
        hasPickable = false;
    }

    public void UnlockHand(int i)
    {
        Debug.Log("Unlocked");
        //if (handConnectedBody[i] != null && handConnectedBody[i].GetComponent<Grabbable>() != null)
        //if (!handConnectedBody[i].GetComponent<Grabbable>().FixedWeight)
        //handConnectedBody[i].GetComponent<Grabbable>().ResetWeight();

        // Destroy hinges
        if (Grabbed[i]) Destroy(hand[i].GetComponent<FixedJoint>());
        if (Picked[i]) Destroy(hand[i].GetComponent<FixedJoint>());

        armControllers[i].DestroyHingeJoints();

        ResetHandValues(i);

        uiManager.EnableGrabFeedback(i, false);

    }

    public void UnlockAllHands()
    {
        if (Grabbed[0]) UnlockHand(0);
        if (Grabbed[1]) UnlockHand(1);
    }

    private void ResetHandValues(int i)
    {
        Rigidbody rb = hand[i].GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.None;
        rb.isKinematic = false;
        throwHand[i] = false;
        throwFlag[i] = false;
        Grabbed[i] = false;
        Picked[i] = false;
        throwTimer[i] = 0;
    }
}
