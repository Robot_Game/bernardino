﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabbable : MonoBehaviour
{
    //public bool FixedWeight => fixedWeight;
    public bool ChangeKinematic => changeKinematic;

    [SerializeField] private bool fixedWeight;
    [SerializeField] private bool changeKinematic;

    private Rigidbody rb;
    //private float initWeight;

    private void Awake()
    {
        //if (!fixedWeight || changeKinematic)
        if (changeKinematic)
        {
            rb = GetComponent<Rigidbody>();
            //initWeight = rb.mass;
        }
    }

    public void ChangeKinematicValue()
    {
        //if (!fixedWeight) rb.mass /= 10;

        if (changeKinematic)
            if(rb.isKinematic) rb.isKinematic = false;
    }

    //public void ResetWeight()
    //{
    //    if(!fixedWeight) rb.mass = initWeight;
    //}
}
