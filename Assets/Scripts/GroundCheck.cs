﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    private PlayerController pc;

    private void Start()
    {
        pc = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Floor" || other.gameObject.layer == 13 || other.gameObject.layer == 17)
        {
            pc.SetRealGroundCheck(true);
            pc.SetStandingValue(true);

        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        pc.SetRealGroundCheck(false);
    }
}
