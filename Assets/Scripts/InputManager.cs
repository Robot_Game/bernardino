﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputManager
{
    public static float Horizontal()
    {
        float r = 0;
        r += Input.GetAxis("K_Horizontal");
        r += Input.GetAxis("J_Horizontal");
        return Mathf.Clamp(r, -1, 1);
    }
    public static float Vertical()
    {
        float r = 0;
        r += Input.GetAxis("K_Vertical");
        r += Input.GetAxis("J_Vertical");
        return Mathf.Clamp(r, -1, 1);
    }
    public static Vector3 MainJoystick()
    {
        return new Vector3(Horizontal(), 0, Vertical());
    }

    public static bool GetButton(string buttonName)
    {
        if (Input.GetButton("K_" + buttonName) || Input.GetButton("J_" + buttonName)) return true;

        return false;
    }

    public static bool GetButtonDown(string buttonName)
    {
        if (Input.GetButtonDown("K_" + buttonName) || Input.GetButtonDown("J_" + buttonName)) return true;

        return false;
    }

    public static float MouseX()
    {
        float r = 0;
        r += Input.GetAxis("M_MouseX");
        r += Input.GetAxis("J_MouseX");
        return Mathf.Clamp(r, -1, 1);
    }

    public static float MouseY()
    {
        float r = 0;
        r += Input.GetAxis("M_MouseY");
        r += Input.GetAxis("J_MouseY");
        return Mathf.Clamp(r, -1, 1);
    }

    public static Vector3 Mouse()
    {
        return new Vector3(MouseX(), 0, MouseY());
    }
}
