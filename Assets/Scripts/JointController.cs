﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointController : MonoBehaviour
{

    public Transform connectedBody;
    [SerializeField] private bool determineDistanceOnStart = true;
    //[SerializeField] private bool inverse = false;
    [SerializeField] private float springForce = 0.1f;
    [SerializeField] private float damper = 20f;
    [SerializeField] private float maxDistance = 0.001f;
    [SerializeField] private float rotationSpeed = 4;
    [SerializeField] private float maxAngle = 30;
    //[SerializeField, Range(-1, 1)] private float rotationDirection = 1;

    //private Transform nextJoint;
    //private Vector3 middlePos;
    private float distance;
    private bool isGrabbing;
    private float springReseter;
    private float damperReseter;
    private float rotationSpeedReseter;
    Quaternion targetRotation;

    protected Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        if (connectedBody == null)
        {
            int index = transform.GetSiblingIndex();
            connectedBody = transform.parent.GetChild(index - 1);
        }
        /*
        if (nextJoint == null)
        {
            int index = transform.GetSiblingIndex();
            if (transform.parent.childCount > index + 1)
                nextJoint = transform.parent.GetChild(index + 1);
        }*/
    }

    void Start()
    {
        if (determineDistanceOnStart && connectedBody != null)
            distance = Vector3.Distance(rb.position, connectedBody.position) / 15;

        springReseter = springForce;
        damperReseter = damper;
        rotationSpeedReseter = rotationSpeed;
    }

    void FixedUpdate()
    {
        if (connectedBody != null)
        {
            if (!isGrabbing)
            {
                ManagePosition();

            }

            ChangeRotation();
        }
    }

    private void Update()
    {
        //SetSoftValues();


    }

    public bool MaxDistanceReached()
    {
        if (Vector3.Distance(rb.position, connectedBody.position) > 10) return true;

        return false;
    }

    private void ManagePosition()
    {
        Vector3 connection = rb.position - connectedBody.position;
        float distanceDiscrepancy = distance - connection.magnitude;

        //rb.position = ((connectedBody.position + nextJoint.position) * 0.5f);
        rb.position += distanceDiscrepancy * connection.normalized;
        //rb.position = ((connectedBody.position + nextJoint.position) * 0.5f);
        Vector3 velocityTarget = connection + (rb.velocity + Physics.gravity * springForce);
        Vector3 projectOnConnection = Vector3.Project(velocityTarget, connection);
        rb.velocity = (velocityTarget - projectOnConnection) / (1 + damper * Time.fixedDeltaTime);

        ClampPosition();
    }

    private void ClampPosition()
    {
        var dst = Vector3.Distance(connectedBody.position, transform.position);
        if (dst > maxDistance)
        {
            Vector3 vect = connectedBody.position - transform.position;
            vect = vect.normalized;
            vect *= (dst - maxDistance);
            transform.position += vect;
        }
    }

    private void SetSoftValues()
    {
        if (Vector3.Distance(rb.position, connectedBody.position) < 0.05f)
        {
            springForce = 0.005f;
            damper = 2;
            rotationSpeed = 0.01f;
        }
        else
        {
            springForce = springReseter;
            damper = damperReseter;
            rotationSpeed = rotationSpeedReseter;
        }
    }

    public void SetIsGrabbingValue(bool value)
    {
        isGrabbing = value;
    }

    /*
    void Start()
    {
        middlePos = (((connectedBody.position + nextJoint.position) * 0.5f) + connectedBody.position)*0.5f;
        if (determineDistanceOnStart && connectedBody != null)
            distance = Vector3.Distance(middlePos, connectedBody.position);
        springForce *= 2;
        damper *= 2;
    }

    private void Update()
    {
        middlePos = (((connectedBody.position + nextJoint.position) * 0.5f) + connectedBody.position) * 0.5f;
    }

    void FixedUpdate()
    {
        if (connectedBody != null)
        {
            ChangeRotation();
            ManageDistance();
        }
    }

    private void ManageDistance()
    {
        Vector3 connection = rb.position - middlePos;
        float distanceDiscrepancy = (distance - connection.magnitude)/10;

        //rb.position = ((connectedBody.position + nextJoint.position) * 0.5f);
        rb.position += distanceDiscrepancy * connection.normalized;
        //rb.position = ((connectedBody.position + nextJoint.position) * 0.5f);

        Vector3 velocityTarget = connection + (rb.velocity + Physics.gravity * springForce);
        Vector3 projectOnConnection = Vector3.Project(velocityTarget, connection);
        rb.velocity = (velocityTarget - projectOnConnection) / (1 + damper * Time.fixedDeltaTime);
    }
    */
    private void ChangeRotation()
    {
        //if (Vector3.Distance(transform.position, nextJoint.position) > 0.1)
        //{
        //transform.LookAt(nextJoint);

        //Vector3 targetDir = connectedBody.position - transform.position;
        Vector3 targetDir = new Vector3(connectedBody.position.x,
                                       connectedBody.transform.position.y,
                                       transform.position.z);
        // The step size is equal to speed times frame time.
        float step;
        //if (rb.velocity.magnitude > 1f)
        //{
            step = rotationSpeed * 10 * Time.deltaTime;
        //}
        //else
        //{
            //step = step = rotationSpeed / 100 * Time.deltaTime;

           // Debug.Log("Slow rotation");
        //}
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 10);
        newDir.z = 0;

        //if (transform.rotation.x > 09 || transform.rotation.x < -90) transform.rotation = Quaternion.identity;
        //if (transform.rotation.y > 90 || transform.rotation.y < -90) transform.rotation = Quaternion.identity;
        //if (transform.rotation.z > 90 || transform.rotation.z < -90) transform.rotation = Quaternion.identity;
        //Vector3 newDir;


        //if (!inverse)  newDir = Vector3.RotateTowards(transform.right, targetDir, step, 0);
        //else  newDir = Vector3.RotateTowards(-transform.right, targetDir, step, 90);
        transform.rotation = QuaternionLookRotation(newDir, Vector3.forward);
        Debug.DrawRay(transform.position, newDir, Color.red);

        // Move our position a step closer to the target.
        transform.rotation = new Quaternion(0, 0, 0, 0); 

        /*Vector3 look = targetDir;
        look.z = 0;
        Quaternion q = Quaternion.LookRotation(look);
        if (Quaternion.Angle(q, connectedBody.rotation) <= maxAngle)
            targetRotation = q;

        //transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);

        transform.rotation = Quaternion.LookRotation(connectedBody.forward);
        transform.LookAt(2 *  connectedBody.position - transform.position);*/



    }

    private void ChangeRotation2()
    {
        //if (Vector3.Distance(transform.position, nextJoint.position) > 0.1)
        //{
        //transform.LookAt(nextJoint);

        Vector3 targetDir = connectedBody.position - transform.position;

        // The step size is equal to speed times frame time.
        float step = rotationSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 90);
        //Vector3 newDir;
        //if (!inverse)  newDir = Vector3.RotateTowards(transform.right, targetDir, step, 0);
        //else  newDir = Vector3.RotateTowards(-transform.right, targetDir, step, 0);
        //transform.rotation = QuaternionLookRotation(newDir, Vector3.forward);
        Debug.DrawRay(transform.position, newDir, Color.red);

        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir, Vector3.up);

    }

    private Quaternion QuaternionLookRotation(Vector3 forward, Vector3 up)
    {
        forward.Normalize();

        Vector3 vector = Vector3.Normalize(forward);
        Vector3 vector2 = Vector3.Normalize(Vector3.Cross(up, vector));
        Vector3 vector3 = Vector3.Cross(vector, vector2);
        float m00 = vector3.x;
        float m01 = vector3.y;
        float m02 = vector3.z;
        float m10 = vector2.x;
        float m11 = vector2.y;
        float m12 = vector2.z;
        float m20 = vector.x;
        float m21 = vector.y;
        float m22 = vector.z;


        float num8 = (m00 + m11) + m22;
        Quaternion quaternion = new Quaternion();
        if (num8 > 0f)
        {
            float num = Mathf.Sqrt(num8 + 1f);
            quaternion.w = num * 0.5f;
            num = 0.5f / num;
            quaternion.x = (m12 - m21) * num;
            quaternion.y = (m20 - m02) * num;
            quaternion.z = (m01 - m10) * num;
            return quaternion;
        }
        if ((m00 >= m11) && (m00 >= m22))
        {
            float num7 = Mathf.Sqrt(((1f + m00) - m11) - m22);
            float num4 = 0.5f / num7;
            quaternion.x = 0.5f * num7;
            quaternion.y = (m01 + m10) * num4;
            quaternion.z = (m02 + m20) * num4;
            quaternion.w = (m12 - m21) * num4;
            return quaternion;
        }
        if (m11 > m22)
        {
            float num6 = Mathf.Sqrt(((1f + m11) - m00) - m22);
            float num3 = 0.5f / num6;
            quaternion.x = (m10 + m01) * num3;
            quaternion.y = 0.5f * num6;
            quaternion.z = (m21 + m12) * num3;
            quaternion.w = (m20 - m02) * num3;
            return quaternion;
        }
        float num5 = Mathf.Sqrt(((1f + m22) - m00) - m11);
        float num2 = 0.5f / num5;
        quaternion.x = (m20 + m02) * num2;
        quaternion.y = (m21 + m12) * num2;
        quaternion.z = 0.5f * num5;
        quaternion.w = (m01 - m10) * num2;
        return quaternion;
    }

}
