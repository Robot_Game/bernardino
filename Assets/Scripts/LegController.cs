﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegController : MonoBehaviour
{


    //[SerializeField] bool reverseMovement;

    private Transform legController;
    private HingeJoint joint;
    private JointSpring js;

    // Start is called before the first frame update
    void Start()
    {
        legController = GameObject.FindGameObjectWithTag("LegController").transform;
        joint = GetComponent<HingeJoint>();
        js = joint.spring;
    }

    // Update is called once per frame
    void Update()
    {
        ManageJointAngle();
    }

    private void ManageJointAngle()
    {
        js.targetPosition = legController.localEulerAngles.x;
        if (js.targetPosition > 180) js.targetPosition -= 360;
        js.targetPosition = Mathf.Clamp(js.targetPosition, joint.limits.min + 5, joint.limits.max - 5);

        //if (reverseMovement) js.targetPosition = js.targetPosition * -1;
        joint.spring = js;
    }
}
