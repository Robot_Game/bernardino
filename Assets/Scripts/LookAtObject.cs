﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtObject : MonoBehaviour
{

    [SerializeField] private Transform target;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float detectionRadius;

    // Start is called before the first frame update
    void Start()
    {
        if (target == null) target = FindObjectOfType<PlayerController>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsInRadius()) LookAtObj();
    }

    private void LookAtObj()
    {
        Vector3 lookPos = target.position - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationSpeed);

    }

    private bool IsInRadius() => Vector3.Distance(target.position, transform.position) < detectionRadius;
}
