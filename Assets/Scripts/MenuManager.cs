﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    [SerializeField] private Transform playButton;
    [SerializeField] private Transform optionsButton;
    [SerializeField] private Transform quitButton;
    [SerializeField] private Transform bernardino;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float force;

    private Vector3 forcePosition;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckForPress();
    }

    private void CheckForPress()
    {
        if (Input.GetMouseButtonDown(0)) CheckForButton();
    }

    private void CheckForButton()
    {
        if (GetButton() == "Play") Play();
        if (GetButton() == "Options") Options();
        if (GetButton() == "Credits") Credits();
        if (GetButton() == "Quit") Quit();
    }

    private string GetButton()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Physics.Raycast(ray, out hit, 200, layerMask);

        forcePosition = hit.point;

        if(hit.collider != null && hit.collider.gameObject.GetComponent<Rigidbody>() != null)
            hit.collider.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.forward * force, forcePosition, ForceMode.Force);

        if (hit.collider != null && hit.collider.transform.parent == playButton)
            return "Play";
        if (hit.collider != null && hit.collider.transform.parent == optionsButton)
            return "Options";
        if (hit.collider != null && hit.collider.transform.parent == quitButton)
            return "Quit";
        if (hit.collider != null && hit.collider.gameObject.layer == 9)
            return "Credits";

        return null;
    }

    private void Play()
    {
        Debug.Log("DEU!");
        playButton.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.forward * force, forcePosition, ForceMode.Force);
        SceneManager.LoadScene("MainScene");
    }

    private void Options()
    {
        optionsButton.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.forward * force, forcePosition, ForceMode.Force);
    }
    private void Credits()
    {
        bernardino.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.forward * force, forcePosition, ForceMode.Force);
    }

    private void Quit()
    {
        quitButton.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.forward * force, forcePosition, ForceMode.Force);
        Application.Quit();
    }
}
