﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OctoController : MonoBehaviour
{
    [SerializeField] private float spawnDistance = 20;
    [SerializeField] private float minSlapCoolDownTime = 2;
    [SerializeField] private float maxSlapCoolDownTime = 4;
    [SerializeField] private float minDefenseCooldownTime = 4;
    [SerializeField] private float maxDefenseCooldownTime = 6;
    [SerializeField] private float normalDefenseDuration = 6;
    [SerializeField] private float hurtDefenseDuration = 9;
    [SerializeField] private float defenseProbability = 0.3f;

    [SerializeField] private float stageDifficultyMultiplier;
    [SerializeField] private Animator tentaclesAnimator;
    [SerializeField] private Animator headAnimator;
    [SerializeField] private Animator doorAnimator;

    [SerializeField] private Transform bigEye;
    [SerializeField] private Transform smallEye;


    // Player
    private Transform player;

    // Head
    private Transform head;
    private RotateToObject headRotationScript;

    // Eyes
    private Animator bigEyeAnim;
    private Animator smallEyeAnim;
    private Quaternion bigEyeInitRotation;
    private Quaternion smallEyeInitRotation;
    private RotateToObject bigEyeRotationScript;
    private RotateToObject smallEyeRotationScript;
    private OctoEyeController bigEyeController;
    private OctoEyeController smallEyeController;

    // Tentacles
    private TentacleController[] tentacleControllers;
    private TentacleController closestTentacle;
    private Transform[] tentacles;
    private int closestTentacleIndex;

    // Booleans
    private bool isSleeping;
    private bool isSlapping;
    private bool isSpinning;
    private bool isHurt;
    private bool multipleAttackers = true;
    private bool isCoolingDownAttack;
    private bool isCoolingDownDefense;

    private int bossStage = 1;

    // Timers
    private float defenseTimer = 0;
    private float defenseTimer2 = 0;
    private float cooldownTimer = 0;

    private Collider[] colliders;

    public bool IsProtected { get; private set; }
    public bool IsAlive { get; private set; } = true;


    // Start is called before the first frame update
    void Start()
    {
        isSleeping = true;

        player = GameObject.FindGameObjectWithTag("Player").transform;

        head = transform.Find("head").transform;

        bigEyeInitRotation = bigEye.Find("eye 1").rotation;
        smallEyeInitRotation = smallEye.Find("eye 1").rotation;

        bigEyeRotationScript = bigEye.GetComponentInChildren<RotateToObject>();
        smallEyeRotationScript = smallEye.GetComponentInChildren<RotateToObject>();
        bigEyeController = bigEye.GetComponentInChildren<OctoEyeController>();
        smallEyeController = smallEye.GetComponentInChildren<OctoEyeController>();
        headRotationScript = head.GetComponent<RotateToObject>();

        bigEyeAnim = bigEye.GetComponent<Animator>();
        smallEyeAnim = smallEye.GetComponent<Animator>();

        GetTentacles();
        //StartCoroutine(AttackCooldown());
        //StartCoroutine(DefenseCooldown());
    }

    private void DisableAllColliders()
    {
        colliders = transform.GetComponentsInChildren<Collider>();
        for(int i = 0; i < colliders.Length; i++)
        {
            colliders[i].enabled = false;
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (isSleeping && 
            Vector3.Distance(transform.position, player.position) < spawnDistance) Spawn();

        if (!isSleeping && IsAlive)
        {
            ManageClosestTentacle();
            ManageAttack();
            ManageDefense();
        }

        if(IsAlive) ManageState();
    }

    private void Spawn()
    {
        if (isSleeping)
        {
            doorAnimator.GetComponent<Animator>().SetTrigger("Close");
            StartCoroutine(PlaySpawnAnim());
            isSleeping = false;

        }
    }

    private IEnumerator PlaySpawnAnim()
    {
        while (doorAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        GetComponent<Animator>().SetTrigger("Spawn");
    }

    private void ManageState()
    {
        if(bigEye.GetComponentInChildren<OctoEyeController>().IsDead
            && smallEye.GetComponentInChildren<OctoEyeController>().IsDead)
        {
            Die();
        }
    }

    private void Die()
    {
        IsAlive = false;

        GetComponent<Animator>().SetTrigger("Die");
        StartCoroutine(OpenDoor());
    }

    private IEnumerator OpenDoor()
    {
        yield return new WaitForSeconds(0.5f);

        while (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        doorAnimator.GetComponent<Animator>().SetTrigger("Open");
    }

    private void ManageAttack()
    {
        if (closestTentacle != null)
        {
            if (!isCoolingDownAttack
                && (!isSlapping || multipleAttackers)
                && closestTentacle.DetectingPlayer
                && !IsProtected)
            {
                // Wait a few seconds before slapping so that the tentacle may face the player
                SlapDown();
            }
        }
    }

    public void ResetAttack()
    {
        isSlapping = false;
    }

    private void ManageDefense()
    {

        bool flag = false;

        if (!isCoolingDownDefense && !IsProtected)
        {
            defenseTimer += Time.deltaTime;
            if (!isSlapping
                && tentaclesAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
            {

                if (defenseTimer > minDefenseCooldownTime)
                {
                    if (Random.Range(0f, 1f) <= defenseProbability) Defend(false);

                    defenseTimer = 0;
                }

            }
        }
        else defenseTimer = 0;

        if (IsProtected)
        {
            defenseTimer2 += Time.deltaTime;
            if (!isHurt && defenseTimer2 >= normalDefenseDuration) flag = true;
            if (isHurt && defenseTimer2 >= hurtDefenseDuration) flag = true;
            if (flag)
            {

                if(!isSpinning)
                {
                    Debug.Log("Opening normal eyes");
                    tentaclesAnimator.speed = 1;
                    CallEyeAnimation("OpenEyes");
                }
                else
                {
                    isSpinning = false;
                    for (int i = 0; i < tentacleControllers.Length; i++)
                    {
                        tentacleControllers[i].TentaclesUp();
                    }
                    Debug.Log("Opening hurt eyes and starting normal movement");
                    CallTentaclesAnimation("NormalMovement");
                    CallEyeAnimation("HurtAnim");

                }

                defenseTimer2 = 0;

                EnableRotation(true);
                bigEyeController.ResetHurtState();
                smallEyeController.ResetHurtState();
                isHurt = false;
                IsProtected = false;
            }
        }
    }

    private void EnableRotation(bool value)
    {
        if(!(bigEyeController.IsDead && !value)) bigEyeRotationScript.enabled = value;
        if (!(smallEyeController.IsDead && !value)) smallEyeRotationScript.enabled = value;
        headRotationScript.enabled = value;
    }

    private void Defend(bool hurt)
    {
        IsProtected = true;
        CallEyeAnimation("CloseEyes");

        EnableRotation(false);

        if (hurt)
        {
            SpinAttack();
            isHurt = true;
        }
        else tentaclesAnimator.speed = 0;
    }

    private void SpinAttack()
    {
        isSpinning = true;

        for (int i = 0; i < tentacleControllers.Length; i++)
        {
            tentacleControllers[i].TentaclesDown();
        }
    }

    private void SlapDown()
    {
        isSlapping = true;
        if (!closestTentacle.IsSlapping)
        {
            closestTentacle.SlapDown();
        }
        if (!multipleAttackers) StartCoroutine(AttackCooldown());
    }

    public void GetHurt()
    {
        if (bossStage == 2) multipleAttackers = true;
        IncrementDifficulty();
        Defend(true);
        bossStage++;
    }

    private void IncrementDifficulty()
    {
        for (int i = 0; i < tentacleControllers.Length; i++)
            tentacleControllers[i].IncrementDifficulty(stageDifficultyMultiplier);

        minSlapCoolDownTime = minSlapCoolDownTime - minSlapCoolDownTime * stageDifficultyMultiplier;
        maxSlapCoolDownTime = maxSlapCoolDownTime - maxSlapCoolDownTime * stageDifficultyMultiplier;
        defenseProbability = defenseProbability - defenseProbability * stageDifficultyMultiplier;
        normalDefenseDuration = normalDefenseDuration + normalDefenseDuration * stageDifficultyMultiplier;

    }


    private void GetTentacles()
    {
        tentacleControllers = FindObjectsOfType<TentacleController>();
        tentacles = new Transform[tentacleControllers.Length];
        for (int i = 0; i < tentacleControllers.Length; i++)
            tentacles[i] = tentacleControllers[i].transform;
    }

    private void ManageClosestTentacle()
    {
        float minDist = 20;
        Vector3 playerPos = player.position;

        for (int i = 0; i < tentacles.Length; i++)
        {
            float dist = Vector3.Distance(tentacles[i].position, playerPos);
            if (dist < minDist)
            {
                closestTentacle = tentacleControllers[i];
                closestTentacleIndex = i;
                minDist = dist;
            }
        }
    }

    private IEnumerator AttackCooldown()
    {
        isCoolingDownAttack = true;
        cooldownTimer = GetRandomCooldown(minSlapCoolDownTime, maxSlapCoolDownTime);
        yield return new WaitForSeconds(cooldownTimer);
        cooldownTimer = 0;
        isCoolingDownAttack = false;
    }

    private IEnumerator DefenseCooldown()
    {
        isCoolingDownDefense = true;
        cooldownTimer = GetRandomCooldown(minDefenseCooldownTime, maxDefenseCooldownTime);
        yield return new WaitForSeconds(cooldownTimer);
        cooldownTimer = 0;
        isCoolingDownDefense = false;
    }

    private float GetRandomCooldown(float min, float max) => Random.Range(min, max);


    public void CallEyeAnimation(string anim)
    {
        if(!bigEye.GetComponentInChildren<OctoEyeController>().IsDead) bigEyeAnim.SetTrigger(anim);
        if (!smallEye.GetComponentInChildren<OctoEyeController>().IsDead) smallEyeAnim.SetTrigger(anim);
    }

    public void CallTentaclesAnimation(string anim)
    {
        tentaclesAnimator.SetTrigger(anim);
    }

    public void CallHeadAnimation(string anim)
    {
        headAnimator.SetTrigger(anim);
    }

    public bool IsSpinning() => isSpinning;

}
