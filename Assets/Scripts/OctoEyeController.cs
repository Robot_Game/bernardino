﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OctoEyeController : MonoBehaviour
{
    public int TimesHit { get; private set; } = 0;
    public bool IsDead { get; private set; } = false;

    [SerializeField] private Material normalEyeMaterial;
    [SerializeField] private Material hurtEyeMaterial;

    private Material[] initMaterials;

    private Renderer rend;

    private OctoController oc;
    private bool isHurt;

    private void Start()
    {
        oc = transform.GetComponentInParent<OctoController>();
        rend = GetComponent<Renderer>();
        initMaterials = rend.materials;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (!IsDead) KillEye();
        }
    }


    public void ResetHurtState()
    {
        isHurt = false;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (!isHurt && !oc.IsProtected && TimesHit < 2)
        {

            //Bernardino's layer is 9
            if (collision.gameObject.layer == 9
            && collision.gameObject.GetComponent<Rigidbody>().velocity.magnitude > 3.8f
            && !oc.IsProtected)
            {
                //Debug.Log(collision.gameObject + " hit the eye with a speed of: " +
                    //collision.gameObject.GetComponent<Rigidbody>().velocity.magnitude);

                TimesHit++;

                if (TimesHit >= 2) KillEye();

                else StartCoroutine(HurtEffect());

                oc.GetHurt();
                isHurt = true;

            }
        }
    }

    private void KillEye()
    {
        IsDead = true;
        transform.parent.GetComponent<Animator>().SetTrigger("Dead");
        initMaterials[0] = hurtEyeMaterial;
        rend.materials = initMaterials;
    }

    private IEnumerator HurtEffect()
    {
        initMaterials[0] = hurtEyeMaterial;
        rend.materials = initMaterials;

        yield return new WaitForSeconds(0.3f);

        initMaterials[0] = normalEyeMaterial;
        rend.materials = initMaterials;
    }
}
