﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public static bool GameIsPaused = false;

    [SerializeField] private GameObject pauseUI;
    [SerializeField] private GameObject optionsUI;
    [SerializeField] private Animator blackScreenAnimator;

    private PlayerController pc;
    private SaveController sc;
    private bool showingOptions;
    

    private void Awake()
    {
        pc = FindObjectOfType<PlayerController>();
        sc = FindObjectOfType<SaveController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ManageInput();
    }

    private void ManageInput()
    {
        if (InputManager.GetButtonDown("Escape") && pc.IsAlive)
        {
            bool flag = false;
            if (GameIsPaused && !showingOptions)
            {
                Resume();
                flag = true;
            }
            if (!GameIsPaused && !showingOptions && !flag)
            {
                Pause();
                flag = true;
            }

            if (GameIsPaused && showingOptions && !flag) Options(false);
        } 
    }

    public void Resume()
    {
        pauseUI.SetActive(false);
        Time.timeScale = 1;
        GameIsPaused = false;
    }

    public void Pause()
    {
        pauseUI.SetActive(true);
        Time.timeScale = 0;
        GameIsPaused = true;
    }

    public void Save()
    {
        sc.SaveProgress();
    }

    public void Options(bool show)
    {
        optionsUI.SetActive(show);
        showingOptions = show;
    }

    public void Quit()
    {
        if (sc.AutoSave) sc.SaveProgress();
        Resume();
        StartCoroutine(QuitAfterBlack());
    }

    private IEnumerator QuitAfterBlack()
    {
        blackScreenAnimator.SetTrigger("FadeOutBlack");
        blackScreenAnimator.SetFloat("deathSpeedMultiplier", 0.4f);
        while (!blackScreenAnimator.GetCurrentAnimatorStateInfo(0).IsName("FadeOutBlack"))
        {
            yield return null;
        }
        while (blackScreenAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        SceneManager.LoadScene(0);
    }
}
