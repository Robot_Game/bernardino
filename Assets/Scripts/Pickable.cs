﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickable : MonoBehaviour
{
    public bool Picked { get; private set; }

    public Transform Target => target;
    public Transform Parent => parent;
    public bool ChangeKinematic => changeKinematic;

    [SerializeField] private Transform target;
    [SerializeField] private Transform parent;
    [SerializeField] private bool changeKinematic;


    private Rigidbody rb;
    private LayerMask initLayer;
    private float initMass;


    // Start is called before the first frame update
    void Start()
    {
        //target = GameObject.Find("Head_J").transform;
        if (parent == null) parent = transform;
        rb = GetComponent<Rigidbody>();
        initMass = rb.mass;
        initLayer = parent.gameObject.layer;
    }

    public void MoveToTarget(float speed)
    {
        float step = speed * Time.deltaTime;
        parent.position = Vector3.MoveTowards(parent.position, target.position, step);
        parent.rotation = Quaternion.RotateTowards(parent.rotation, target.rotation, step * 10);
        //if(rb != null) rb.MovePosition(target.position);
    }

    public void ChangeKinematicValue()
    {
        //if (!fixedWeight) rb.mass /= 10;

        if (changeKinematic)
            if (rb.isKinematic) rb.isKinematic = false;
    }

    public void ChangeLayer(bool equipping)
    {
        if (equipping) parent.gameObject.layer = 18;
        else parent.gameObject.layer = initLayer;

    }

    public void SetPickedBool(bool value)
    {
        Picked = value;
    }

    public void Lock()
    {
        transform.position = target.position;
        transform.rotation = target.rotation;
        FixedJoint grabHinge = parent.gameObject.AddComponent<FixedJoint>();
        grabHinge.connectedBody = target.GetComponent<Rigidbody>();
        rb.mass = 0;
    }

    public void Unlock()
    {
        FixedJoint grabHinge = parent.gameObject.GetComponent<FixedJoint>();
        if (grabHinge != null) Destroy(grabHinge);
        rb.mass = initMass;
    }
}
