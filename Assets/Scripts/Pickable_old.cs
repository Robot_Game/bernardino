﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickable_old : MonoBehaviour
{
    private PlayerController pc;
    private Vector3 rotation;
    private Vector3 moveVector;
    private bool isPicked;
    private bool flag;
    private float waitTime;

    // Start is called before the first frame update
    void Awake()
    {
        waitTime = 0.05f;
        pc = FindObjectOfType<PlayerController>();
        //StartCoroutine(GetRotation(waitTime));
    }

    private void Update()
    {
        if(pc.VAxis != 0 || pc.HAxis != 0)
        moveVector = pc.MoveVector;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (isPicked && pc.IsMoving) PointUp();
        if (isPicked && pc.IsMoving && !flag) Activate(true);
        if (isPicked && !pc.IsMoving && flag) Activate(false);

        if (isPicked && !flag) PointUp();
    }

    private void RandomRotation()
    {
        transform.rotation = Quaternion.Slerp(
        transform.rotation, Quaternion.LookRotation(rotation), 0.15f);
    }

    private IEnumerator GetRotation(float waitTime)
    {
        while (true)
        {
            rotation = Random.onUnitSphere;
            yield return new WaitForSeconds(waitTime);
        }
    }

    public void Picked(bool value)
    {
        if (value)
        {
            isPicked = true;
        }
        else
        {
            isPicked = false;
        }
    }

    private void Activate(bool value)
    {
        if (value)
        {
            StartCoroutine(GetRotation(waitTime));
            flag = true;
        }
        else
        {
            StopCoroutine(GetRotation(waitTime));
            flag = false;
        }
    }

    private void PointUp()
    {
        //transform.LookAt(new Vector3(transform.position.x, transform.position.y + 5, transform.position.z));
        //transform.Rotate(90, 0, 0, Space.Self);

        //Vector3 direction = new Vector3(transform.position.x, transform.position.y + 5, transform.position.z) - transform.position;
        //Quaternion toRotation = Quaternion.LookRotation(transform.forward, direction) * pc.transform.rotation;
        //transform.rotation = Quaternion.Slerp(transform.rotation, toRotation, 1f * Time.deltaTime);
        //Vector3 eulerRotation = new Vector3(transform.eulerAngles.x, pc.transform.eulerAngles.y, transform.eulerAngles.z);
            //transform.rotation = Quaternion.Euler(eulerRotation);
        transform.rotation = Quaternion.Slerp(
            transform.rotation, Quaternion.LookRotation(moveVector), 0.15f);

    }
}
