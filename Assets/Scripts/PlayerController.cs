﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Vector3 MoveVector { get; private set; }
    public float HAxis { get; private set; }
    public float VAxis { get; private set; }
    public bool IsMoving { get; private set; } = false;
    public bool IsGrounded { get; private set; } = false; // True if 
    public bool IsAlive { get; private set; } = true;
    public Transform ball;

    [Header("General")]
    [SerializeField] private float maxMoveSpeed = 4;
    [SerializeField] private float maxRunSpeed = 6;
    [SerializeField] private float speedAcceleration = 2;
    [SerializeField] private float jumpPower = 90;
    [SerializeField] private float gravitySmallMultiplier = 1;
    [SerializeField] private float gravityBigMultiplier = 8;
    [SerializeField] private PhysicMaterial playerPhysicsMat; //try to change so in some cases he has no friction and other cases he has
    [Header("SlingShot")]
    [SerializeField] private float minSlingshotDistance = 4f;
    [SerializeField] private float slingshotXSpeed = 50f;
    [SerializeField] private float slingshotYSpeed = 70f;
    [SerializeField] private float slingshotTime = 1f;
    [Header("Elasticity")]
    [SerializeField] private float minElasticForceDistance = 2f;
    [SerializeField] private float maxElasticForceDistance = 6f;
    [SerializeField] private float forceSpeed = 50f;
    [Header("Ragdoll")]
    [SerializeField] private float resistance = 2f;
    [SerializeField] private float knockedDownTime = 2f;
    [Header("Death")]
    [SerializeField] private float slowDownSpeed = 4;
    [SerializeField] private float minDeathTime = 4;
    [SerializeField] private float animationSpeedMultiplier = 2;



    // Slingshot
    private Vector3 slingDirection = Vector3.zero;
    private bool activeSlingshot;
    private bool slingShotFlag;
    private Coroutine coroutine;

    //Elasticity and Swing
    private Vector3 forceDirection = Vector3.zero;
    private bool activeForce;
    private bool forceFlag;
    private bool killSwitch;

    // Ragdoll
    private bool isStanding = false; // True if bernardino isn't knockedDown
    private bool knockedDown;
    private bool knockedDownFlag;
    private float knockedDownTimer;


    // Jump
    private Vector3 increasedGravity;
    private bool gravityFlag;
    private bool jumped;

    // Movement
    private float moveSpeed;
    private float initialMaxSpeed;
    private bool realIsGrounded;    // True if ground check collider is touching the ground

    // Death
    private bool isRespawning = false;
    private float timeScale = 1;
    private float deathTimer = 0;

    // Initial conditions
    private Quaternion initRotation;

    // Materials
    private bool physicsMatFlag;

    // Animation
    private bool legAnimFlag;

    // Components
    private Rigidbody rigidBody;
    private CapsuleCollider capsCol;
    private Animator legAnimator;
    private Animator canvasAnimator;
    private Animator blackScreenAnimator;
    private Camera mainCam;

    // Controllers
    private CameraController camController;
    private GrabController gc;
    private ArmController[] armControllers;
    private SaveController saveController;
    private UIManager uiManager;


    void Start()
    {
        GetComponents();
        GetControllers();
        initialMaxSpeed = maxMoveSpeed;
        initRotation = transform.rotation;
    }

    private void GetComponents()
    {
        rigidBody = GetComponent<Rigidbody>();

        capsCol = GetComponent<CapsuleCollider>();

        legAnimator = GameObject.FindGameObjectWithTag("LegController").GetComponent<Animator>();

        canvasAnimator = GameObject.Find("UIManager").GetComponent<Animator>();

        blackScreenAnimator = canvasAnimator.transform.Find("Canvas").transform.Find("BlackScreen").GetComponent<Animator>();

        mainCam = Camera.main;
    }

    private void GetControllers()
    {
        // Get the controller for each arm
        armControllers = new ArmController[2];
        armControllers[0] = transform.parent.Find("LeftArm").GetComponent<ArmController>();
        armControllers[1] = transform.parent.Find("RightArm").GetComponent<ArmController>();

        gc = GetComponent<GrabController>();

        camController = FindObjectOfType<CameraController>();

        saveController = FindObjectOfType<SaveController>();

        uiManager = FindObjectOfType<UIManager>();
    }

    private void Update()
    {
        if (IsAlive)
        {
            gc.ManageDistance();
            ManageMovementSpeed();
            ManageRagdollState();
            gc.ReadGrabInput();
            ReadJumpInput();
            ManageSlingShot();
            ManageElasticity();
            ManageJointsState();
            SetLegAnimationState();
            ManagePhysicsMaterial();

            if (Input.GetKeyDown(KeyCode.K)) Die();
        }
        else
        {
            SlowDownTime();
            deathTimer += Time.deltaTime;

            if (deathTimer >= minDeathTime)
            {
                if (isRespawning && blackScreenAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
                {
                    //if(canvasAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
                    Respawn();
                    deathTimer = 0;

                }
                if (Input.anyKeyDown && !isRespawning)
                {
                    isRespawning = true;
                    IncrementAnimationSpeed(); //initFadeSpeed = animation.speed;
                    deathTimer = minDeathTime;
                }

            }
        }
        //if (InputManager.GetJumpButtonDown() && caralhoDoFreixo)
        //{
        //    camController.ActivateCameraSwitch(GameObject.Find("MainCamera").transform, true);
        //}
        //if (InputManager.GetJumpButtonDown() && !caralhoDoFreixo)
        //{
        //    camController.ActivateCameraSwitch(GameObject.Find("OctopusCamera").transform, false);
        //    caralhoDoFreixo = true;
        //}


    }

    void FixedUpdate()
    {
        if (!knockedDown && !slingShotFlag && IsAlive)
        {
            Move();
            if(knockedDownTimer == 0) Rotate();
        }

        if(IsAlive) gc.ManageGrab();
    }

    #region Move and Rotate
    private void Move()
    {

        /* Rigidbody based movement v1 */

        if (!isStanding && !slingShotFlag && !activeForce && !forceFlag)
            rigidBody.velocity = new Vector3(MoveVector.x * moveSpeed, rigidBody.velocity.y, MoveVector.z * moveSpeed);
        else if (isStanding && !slingShotFlag && !activeForce && !forceFlag)
            rigidBody.position += MoveVector * moveSpeed * Time.deltaTime;
    }

    private void Rotate()
    {
        if (!activeSlingshot)
        {

            if (IsMoving && !(gc.Grabbed[0] || gc.Grabbed[1]))
                transform.rotation = Quaternion.Slerp(
                    transform.rotation, Quaternion.LookRotation(MoveVector), 0.15f);
            else
            {
                if (gc.Grabbed[0] && !gc.Grabbed[1]
                    && gc.GrabbedHandDistance > 0.2f)
                {
                    Vector3 lookDirection = new Vector3(gc.hand[0].position.x, 0, gc.hand[0].position.z) - new Vector3(transform.position.x, 0, transform.position.z);
                    transform.rotation = Quaternion.Slerp(
                      transform.rotation,
                      Quaternion.LookRotation(lookDirection, Vector3.up),
                      0.3f);
                }
                if (!gc.Grabbed[0] && gc.Grabbed[1]
                    && gc.GrabbedHandDistance > 0.2f)
                {
                    Vector3 lookDirection = new Vector3(gc.hand[1].position.x, 0, gc.hand[1].position.z) - new Vector3(transform.position.x, 0, transform.position.z);
                    transform.rotation = Quaternion.Slerp(
                      transform.rotation,
                      Quaternion.LookRotation(lookDirection, Vector3.up),
                      0.3f);
                }
            }

            if (gc.Grabbed[0] && gc.Grabbed[1]
                && gc.GrabbedHandDistance > 1f)
            {
                Vector3 lookDirection = new Vector3(gc.MiddlePos.x, 0, gc.MiddlePos.z) - new Vector3(transform.position.x, 0, transform.position.z);
                transform.rotation = Quaternion.Slerp(
                  transform.rotation,
                  Quaternion.LookRotation(lookDirection, Vector3.up),
                  0.3f);
            }

        }
    }
    #endregion

    #region Slingshot
    private void ManageSlingShot()
    {

        if (gc.Grabbed[0] && gc.Grabbed[1] && gc.AverageDistance > minSlingshotDistance && !activeSlingshot)
        {
            if (InputManager.GetButtonDown("Jump"))
            {
                Debug.Log("SLING!");
                //slingDirection = new Vector3(transform.forward.x, transform.forward.y, transform.forward.z);
                slingDirection = (gc.MiddlePos - transform.position);
                //if (slingDirection.y < 2.2f)
                //{
                    slingDirection = new Vector3(slingDirection.x, slingDirection.y + 3, slingDirection.z);
                //}
                activeSlingshot = true;
                isStanding = false;

                coroutine = StartCoroutine(ResetSlingshotFlag(slingshotTime));
            }
        }

        if (activeSlingshot && !InputManager.GetButton("Jump"))
        {
            if (coroutine != null) StopCoroutine(coroutine);
            activeSlingshot = false;
        }

        if (activeSlingshot)
        {
            Debug.Log("Slinging!");
            if (!isStanding && !slingShotFlag) slingShotFlag = true;

            Vector3 force;
            /*if (gc.AverageDistance > 10)
                force = new Vector3(
                    slingDirection.x * (gc.AverageDistance / 10) * slingshotXSpeed,
                    slingshotYSpeed,
                    slingDirection.z * (gc.AverageDistance / 10) * slingshotXSpeed);
            else
                force = new Vector3(
                    slingDirection.x * slingshotXSpeed,
                    slingshotYSpeed,
                    slingDirection.z * slingshotXSpeed);*/
            force = Vector3.zero;

            //if (slingDirection.x > 2.2 || slingDirection.z > 2.2)
            //{
                Debug.Log(slingDirection.y);
                Debug.Log("Normal sling!");
                force = new Vector3(
                slingDirection.x * slingshotXSpeed,//(gc.AverageDistance * slingshotXSpeed),
                slingDirection.y * slingshotYSpeed,//(gc.AverageDistance * slingshotYSpeed),
                slingDirection.z * slingshotXSpeed); //(gc.AverageDistance * slingshotXSpeed));
            //}

            //else
            //{
             //   Debug.Log("Up sling!");
             //   force = new Vector3(
             //   slingDirection.x * slingshotXSpeed * 0.6f,//(gc.AverageDistance * slingshotXSpeed),
             //   slingDirection.y * slingshotYSpeed * 0.9f,//(gc.AverageDistance * slingshotYSpeed),
             //   slingDirection.z * slingshotXSpeed * 0.6f); //(gc.AverageDistance * slingshotXSpeed));
           // }
            rigidBody.AddForce(force, ForceMode.Acceleration);
        }
    }

    private IEnumerator ResetSlingshotFlag(float timeToReset)
    {
        // wait 2 thirds of the time
        yield return new WaitForSeconds(timeToReset * 2 / 3);

        //gc.UnlockAllHands();
        KnockDown();
        // wait another third of the time (100% in total)
        yield return new WaitForSeconds(timeToReset / 3);

        activeSlingshot = false;

    }
    #endregion

    #region Swing and Elasticity
    private void ManageElasticity()
    {
        forceDirection = gc.CurrentGrabbedPivot.position - transform.position;

        if (//gc.GrabbedHandDistance > minElasticForceDistance && 
            (gc.Grabbed[0] || gc.Grabbed[1])
            && !activeSlingshot
            && !activeForce)
        {
            activeForce = true;
            if (maxMoveSpeed == initialMaxSpeed)
            {
                maxMoveSpeed *= 10;
                maxRunSpeed *= 10;
            }
        }

        if (activeForce && (gc.GrabbedHandDistance < minElasticForceDistance
            || !(gc.Grabbed[0] || gc.Grabbed[1])
            || killSwitch
            //|| (IsMoving && jumped)
            || (IsMoving && isStanding && realIsGrounded && IsGrounded
            && gc.GrabbedHandDistance < maxElasticForceDistance)))
        {
            activeForce = false;
            if (maxMoveSpeed != initialMaxSpeed)
            {
                maxMoveSpeed /= 10;
                maxRunSpeed /= 10;
            }
        }

        if (activeForce)
        {
            float speed;

            forceFlag = true;

            if (gravityFlag) ResetGravity();

            Vector3 force;
            if (gc.Grabbed[0] && gc.Grabbed[1] && !IsGrounded)
                speed = forceSpeed * gc.GrabbedHandDistance;
            if (gc.Grabbed[0] && gc.Grabbed[1] && IsGrounded)
                speed = forceSpeed * 5 * gc.GrabbedHandDistance;

            if (!IsGrounded)
            {
                if (gc.GrabbedHandDistance < 4)
                    speed = forceSpeed * 8 * (gc.GrabbedHandDistance) / 2;
                else
                    speed = forceSpeed * 8 * ((gc.GrabbedHandDistance));

                if (IsMoving)
                {
                    Vector3 moveForce = MoveVector * forceSpeed * 10;
                    rigidBody.AddForce(moveForce, ForceMode.Force);
                }
            }
            else speed = forceSpeed * (gc.GrabbedHandDistance + 4) * 2;

            if (speed > 130) speed = 130;

            force = forceDirection * speed;

            rigidBody.AddForce(force, ForceMode.Force);
        }
    }
    #endregion

    #region Jump
    private void ReadJumpInput()
    {
        /* Rigidbody based jump */
        if (InputManager.GetButtonDown("Jump") && isStanding && IsGrounded && !knockedDown && !slingShotFlag)
        {
            if (gc.AverageDistance < minSlingshotDistance && gc.Grabbed[0] && gc.Grabbed[1])

                Jump(jumpPower);

            if (!(gc.Grabbed[0] && gc.Grabbed[1]))

                Jump(jumpPower);
        }

        //rigidBody.AddForce(new Vector3(0, jumpPower, 0), ForceMode.Impulse);
        if (rigidBody.velocity.y < 0 && !activeForce && jumped)

            IncreaseGravity(gravitySmallMultiplier);


        if (rigidBody.velocity.y > 0 && !InputManager.GetButton("Jump") && !slingShotFlag
            && !activeForce && jumped)

            IncreaseGravity(gravityBigMultiplier);

    }

    private void Jump(float force)
    {
        rigidBody.velocity += Vector3.up * jumpPower;
        isStanding = false;
        jumped = true;
        StartCoroutine(ResetJumpBoolean());
    }

    private void IncreaseGravity(float multiplier)
    {
        increasedGravity += Vector3.up * Physics.gravity.y * multiplier * Time.deltaTime;
        rigidBody.velocity += Vector3.up * Physics.gravity.y * multiplier * Time.deltaTime;
        gravityFlag = true;
    }

    private void ResetGravity()
    {
        rigidBody.velocity += -increasedGravity;
        increasedGravity = Vector3.zero;
        gravityFlag = false;
    }

    private IEnumerator ResetJumpBoolean()
    {
        yield return new WaitForSeconds(2);

        //jumped = false;
    }

    public void SetStandingValue(bool value)
    {
        increasedGravity = Vector3.zero;
        isStanding = value;
        jumped = !value;
        if (!activeForce) forceFlag = false;
        slingShotFlag = false;
    }

    public void SetRealGroundCheck(bool value)
    {
        realIsGrounded = value;
        IsGrounded = value;
    }

    #endregion

    #region Ragdoll
    private void ManageRagdollState()
    {
        if (knockedDown)
        {
            if (IsGrounded) knockedDownTimer += Time.deltaTime;
            if ((InputManager.GetButtonDown("Jump")
                || InputManager.MainJoystick() != Vector3.zero
                && knockedDownTimer > 0.5f)
                //&& knockedDownTimer > knockedDownTime/3
                && IsGrounded)
                knockedDownTimer += knockedDownTime;
            if (knockedDownTimer > knockedDownTime && IsGrounded && !slingShotFlag) GetUp();
        }

        if (Input.GetButtonDown("Ragdoll")) KnockDown();
    }
    private void KnockDown()
    {
        //capsCol.enabled = false;
        rigidBody.constraints = RigidbodyConstraints.None;
        knockedDown = true;
    }

    private void GetUp()
    {
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
        if (!knockedDownFlag)
        {
            //capsCol.enabled = true;
            //if (knockedDownTimer >= 1) Jump(jumpPower / 20000);
            knockedDownFlag = true;
        }

        //transform.rotation = Quaternion.AngleAxis(-transform.rotation.x, transform.right); //correto!!!!!!!!!!!!!!!!!
        StartCoroutine(ResetKnockDownBoolean());
        StartCoroutine(GetUpRotation()); //amazing
        //transform.rotation = Quaternion.Euler(0, transform.rotation.y, 0);
        //float step = 10 * Time.deltaTime;
        //Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, transform.rotation.y, 0), step);

        //transform.Rotate(Vector3.right * 500 * Time.deltaTime);
    }

    private IEnumerator GetUpRotation()
    {

        while (!(transform.localRotation.x > -0.01f && transform.localRotation.x < 0.01f
            && transform.localRotation.z > -0.01f && transform.localRotation.z < 0.01f))
        {
            transform.rotation = Quaternion.Slerp(
                transform.rotation, Quaternion.LookRotation(MoveVector), 0.5f * Time.deltaTime);

            yield return null;
        }

        //capsCol.enabled = true;

        StartCoroutine(ResetKnockDownTimer());
        StopCoroutine(GetUpRotation());

        yield return null;
    }

    private IEnumerator ResetKnockDownBoolean()
    {
        yield return new WaitForSeconds(0.3f);

        knockedDown = false;
        knockedDownFlag = false;

        StopCoroutine(ResetKnockDownBoolean());
    }

    private IEnumerator ResetKnockDownTimer()
    {
        yield return new WaitForSeconds(0.2f);

        knockedDown = false;
        knockedDownTimer = 0;
        StopCoroutine(ResetKnockDownTimer());
    }

    private void SetLegAnimationState()
    {
        if (legAnimFlag && (!isStanding || !IsGrounded || knockedDown || slingShotFlag) || !IsMoving)
        {
            legAnimator.SetBool("Move", false);
            legAnimFlag = false;
        }
        if (!legAnimFlag && isStanding && IsGrounded && !knockedDown && !slingShotFlag && IsMoving)
        {
            legAnimator.SetBool("Move", true);
            legAnimFlag = true;
        }

        legAnimator.SetFloat("Speed", moveSpeed / 2);
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer != 10 && col.gameObject.layer != 9)
        {
            if (col.relativeVelocity.magnitude > resistance && knockedDownTimer <= 0)
            {
                KnockDown();
            }
        }
    }

    private void OnCollisionStay(Collision col)
    {
        if (col.transform.tag == "Floor")
        {
            IsGrounded = true;
            slingShotFlag = false; // adicionado porque nao conseguia levantar-me em certas situações
            if (!activeForce) forceFlag = false;
            if (killSwitch) killSwitch = false;
        }
    }

    private void ManagePhysicsMaterial()
    {
        if (knockedDown && (gc.Grabbed[0] || gc.Grabbed[1]) && !physicsMatFlag)
        {
            playerPhysicsMat.dynamicFriction = 0;
            playerPhysicsMat.staticFriction = 0;
            playerPhysicsMat.frictionCombine = PhysicMaterialCombine.Minimum;
            physicsMatFlag = true;
        }

        if (!(gc.Grabbed[0] || gc.Grabbed[1]) && physicsMatFlag)
        {
            playerPhysicsMat.dynamicFriction = 0.6f;
            playerPhysicsMat.staticFriction = 0.6f;
            playerPhysicsMat.frictionCombine = PhysicMaterialCombine.Average;
            physicsMatFlag = false;
        }
    }

    #endregion

    #region Distance And Speed

    private void ManageMovementSpeed()
    {
        //camera forward and right vectors:
        Vector3 forward = mainCam.transform.forward;
        Vector3 right = mainCam.transform.right;

        // Ignore Y position of the camera
        forward.y = 0f;
        right.y = 0f;
        forward.Normalize();
        right.Normalize();

        HAxis = InputManager.Horizontal();
        VAxis = InputManager.Vertical();
        MoveVector = VAxis * forward + HAxis * right;

        // Speed Calculation Formula
        // y = m * (x-x1) + y1
        // y = [(y2 - y1)/(x2-x1)]*(x-x1) + y1

        // Walk
        if (!Input.GetButton("Run"))
        {
            SetSlingShotSpeed(maxMoveSpeed);
            SetGrabbedSpeed(maxMoveSpeed, "Walk");
        }
        // Run
        else
        {
            SetSlingShotSpeed(maxRunSpeed);
            SetGrabbedSpeed(maxRunSpeed, "Run");

        }

        if (InputManager.MainJoystick() != Vector3.zero) IsMoving = true;
        else IsMoving = false;
    }

    private void SetSlingShotSpeed(float speed)
    {
        if (gc.Grabbed[0] && gc.Grabbed[1]
            && !gc.IsMovingToHand(MoveVector, transform, 90))

            moveSpeed = (-(speed / (gc.MaxGrabDistance - gc.OffsetDistance))) * (gc.AverageDistance) + speed;

        if (gc.Grabbed[0] && gc.Grabbed[1]
            && gc.IsMovingToHand(MoveVector, transform, 90))

            moveSpeed = speed;
    }

    private void SetGrabbedSpeed(float speed, string moveType)
    {
        //One arm: Walking Back
        if (((gc.Grabbed[0] && !gc.Grabbed[1]) || (!gc.Grabbed[0] && gc.Grabbed[1]))
            && !gc.IsMovingToHand(MoveVector, transform, 110))

            moveSpeed = (-(speed / (gc.MaxGrabDistance - gc.OffsetDistance / 2))) * (gc.GrabbedHandDistance) + speed;

        //One arm: Walking Forward
        if (((gc.Grabbed[0] && !gc.Grabbed[1]) || (!gc.Grabbed[0] && gc.Grabbed[1]))
            && gc.IsMovingToHand(MoveVector, transform, 110) && moveSpeed < speed / 2)
        {

            if (moveType == "Walk") moveSpeed += speedAcceleration / 100;
            if (moveType == "Run") moveSpeed += speedAcceleration / 200;
        }


        if (!gc.Grabbed[0] && !gc.Grabbed[1] && moveSpeed > speed)
        {

            if (moveType == "Walk") moveSpeed -= speedAcceleration / 50;
            if (moveType == "Run") moveSpeed = maxRunSpeed;
        }

        if (!gc.Grabbed[0] && !gc.Grabbed[1] && moveSpeed < speed
        && (VAxis != 0 || HAxis != 0))
        {
            if (moveType == "Walk") moveSpeed += speedAcceleration / 50;
            if (moveType == "Run") moveSpeed += speedAcceleration / 200;
        }
    }

    private void ManageJointsState()
    {
        if (armControllers[0].MaxDistanceReached() || armControllers[1].MaxDistanceReached() && !killSwitch)
        {
            Rigidbody[] rbs = transform.GetComponentsInChildren<Rigidbody>(true);
            HingeJoint[] joints = transform.GetComponentsInChildren<HingeJoint>(true);
            JointsKillSwitch(rbs, joints);
            killSwitch = true;
        }

    }

    private void JointsKillSwitch(Rigidbody[] rbs, HingeJoint[] joints)
    {
        for (int i = 0; i < rbs.Length; i++)
        {
            rbs[i].isKinematic = true;
            rbs[i].velocity = Vector3.zero;

        }
        //for (int i = 0; i < joints.Length; i++)
        //{
        //    joints[i].
        //}

        if (gc.Grabbed[0]) gc.UnlockHand(0);

        armControllers[0].ResetInitialPositions();

        if (gc.Grabbed[1]) gc.UnlockHand(1);
        armControllers[1].ResetInitialPositions();

        StartCoroutine(ResetRigidBodies(rbs));
    }

    private IEnumerator ResetRigidBodies(Rigidbody[] rbs)
    {
        yield return new WaitForSeconds(0.05f);

        for (int i = 0; i < rbs.Length; i++)
        {
            rbs[i].isKinematic = false;
            rbs[i].velocity = Vector3.zero;

        }
    }
    #endregion

    #region Die and Respawn
    public void Die()
    {
        IsAlive = false;
        gc.UnlockAllHands();
        if (!knockedDown) KnockDown();
        uiManager.ChangeDeathScreen();
        canvasAnimator.SetTrigger("DeathScreen");
    }

    private void SlowDownTime()
    {
        if (timeScale > 0.2f)
        {
            timeScale -= slowDownSpeed / 100;
        }
        if (timeScale < 0) timeScale = 0;

        Time.timeScale = timeScale;
    }

    private void ResetTimescale()
    {
        timeScale = 1;
        Time.timeScale = 1;
    }

    private void IncrementAnimationSpeed()
    {
        canvasAnimator.SetFloat("deathSpeedMultiplier", animationSpeedMultiplier);
        blackScreenAnimator.SetFloat("deathSpeedMultiplier", animationSpeedMultiplier);
        blackScreenAnimator.SetTrigger("FadeOutBlack");
    }

    private void Respawn()
    {
        ResetTimescale();
        transform.position = saveController.currentCheckpoint.position;
        transform.rotation = initRotation;
        GetUp();
        IsAlive = true;
        canvasAnimator.SetFloat("deathSpeedMultiplier", 1);
        canvasAnimator.SetTrigger("FadeOutBlack");
        blackScreenAnimator.SetFloat("deathSpeedMultiplier", 1);
        blackScreenAnimator.SetTrigger("FadeInBlack");
        isRespawning = false;
    }
    #endregion
}