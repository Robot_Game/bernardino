﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RenderRopeLines : MonoBehaviour
{
    [SerializeField] private float lineSize = 0.2f;
    [SerializeField] private Transform target;
    [SerializeField] private Material material;

    private LineRenderer line;

    // Use this for initialization
    void Start()
    {
        SetParameters();

        DrawLine();
    }

    // Update is called once per frame
    void Update()
    {
        DrawLine();
    }

    private void SetParameters()
    {
        transform.GetComponent<MeshRenderer>().enabled = false;

        // Create or Get LineRenderer
        line = gameObject.GetComponent<LineRenderer>();
        if (line == null && transform.GetSiblingIndex() != transform.parent.childCount - 1) line = gameObject.AddComponent<LineRenderer>();
        if (line != null && transform.GetSiblingIndex() == transform.parent.childCount - 1) DestroyImmediate(gameObject.GetComponent<LineRenderer>());

        if (line != null)
        {
            // Set the width of the Line Renderer
            line.startWidth = lineSize;
            line.endWidth = lineSize;

            // Set the number of vertex fo the Line Renderer
            line.positionCount = 2;

            line.material = material;
        }

        transform.localPosition = new Vector3(0, -1.4f * transform.GetSiblingIndex(), 0);
        if(GetComponent<HingeJoint>() != null && transform.GetSiblingIndex() != 0) GetComponent<HingeJoint>().connectedBody = transform.parent.GetChild(transform.GetSiblingIndex() - 1).GetComponent<Rigidbody>();
        if (transform.GetSiblingIndex() != transform.parent.childCount - 1) target = transform.parent.GetChild(transform.GetSiblingIndex() + 1);
    }

    private void DrawLine()
    {
        // Check if the GameObjects are not null
        if (target != null)
        {
            // Update position of the two vertex of the Line Renderer
            line.SetPosition(0, transform.position);
            line.SetPosition(1, target.position);
        }
    }
}
