﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToObject : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private bool rotateOnOneAxis;
    [SerializeField] private string axis;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
    }

    private void Rotate()
    {
        Vector3 lookPos = target.position - transform.position;
        if(rotateOnOneAxis)
        {
            switch (axis)
            {
                case "x":
                    lookPos.x = 0;
                    break;
                case "y":
                    lookPos.y = 0;
                    break;
                case "z":
                    lookPos.z = 0;
                    break;
            }
        }
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationSpeed);
    }
}
