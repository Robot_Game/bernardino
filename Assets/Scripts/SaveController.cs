﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveController : MonoBehaviour
{
    public bool AutoSave = true;

    public Transform currentCheckpoint { get; private set; }

    private void Start()
    {
        currentCheckpoint = GameObject.Find("Checkpoint 0").transform;
    }

    public void SetCheckpoint(Transform checkpoint)
    {
        currentCheckpoint = checkpoint;
    }

    public void SaveProgress()
    {

    }

    private void LoadGame()
    {

    }
}
