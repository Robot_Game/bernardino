﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleController : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private Transform tentacleBase;
    [SerializeField] private Transform tentacleJoint;
    [SerializeField] private float detectionRadius;
    [SerializeField] private float rotationSpeed;
    [Header("Slap")]
    [SerializeField] private Vector3 rotationAxis;
    [SerializeField] private float preAttackAngle;
    [SerializeField] private float rotationAngle;
    [SerializeField] private float preAttackTime;
    [SerializeField] private float attackTime;
    [SerializeField] private float waitTime;
    [SerializeField] private float resetTime;


    private OctoController oc;
    private Quaternion initialRot;
    private Transform player;
    private bool rotationFlag = true;
    private bool resetFlag;

    public bool IsSlapping { get; private set; }
    public bool IsDown { get; private set; }
    public bool DetectingPlayer { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        oc = FindObjectOfType<OctoController>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        initialRot = tentacleBase.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        DetectingPlayer = IsInRadius();
        if (IsInRadius(14) && rotationFlag &&
            !oc.IsProtected) RotateToPlayer();

        if (resetFlag) ResetInitialRotation();
    }

    public void IncrementDifficulty(float multiplier)
    {
        rotationSpeed += rotationSpeed * multiplier;
        preAttackTime -= preAttackTime * multiplier;
        attackTime -= attackTime * multiplier;
        attackTime -= attackTime * multiplier;
        waitTime -= waitTime * multiplier;
        resetTime -= resetTime * multiplier;
    }

    private void RotateToPlayer()
    {
        Vector3 lookPos = player.position - tentacleBase.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        tentacleBase.rotation = Quaternion.Slerp(tentacleBase.rotation, rotation, Time.deltaTime * rotationSpeed);
        if (!IsSlapping) tentacleJoint.rotation = tentacleBase.rotation;
    }

    public void SlapDown()
    {
        //rotate 90 degrees in Y
        StartCoroutine(Slap());
    }

    public void TentaclesDown()
    {
        resetFlag = true;
    }

    public void TentaclesUp()
    {
        //rotate 90 degrees in Y
        StartCoroutine(GoUp());
    }

    private void ResetInitialRotation()
    {
        tentacleBase.rotation = Quaternion.Slerp(tentacleBase.rotation, initialRot, Time.time * (rotationSpeed/4));

        if(Quaternion.Angle(tentacleBase.rotation, initialRot) < 0.5f)
        {
            tentacleBase.rotation = initialRot;
            StartCoroutine(GoDown());
            resetFlag = false;
        }
    }

    private IEnumerator GoDown()
    {
        rotationFlag = false;
        IsSlapping = true;
        IsDown = true;
        Quaternion from = tentacleJoint.rotation;
        Quaternion to = tentacleJoint.rotation;
        to *= Quaternion.Euler(rotationAxis * (rotationAngle));

        float elapsed = 0.0f;
        while (elapsed < resetTime)
        {
            tentacleJoint.rotation = Quaternion.Slerp(from, to, elapsed / resetTime);
            elapsed += Time.deltaTime;
            yield return null;
        }
        tentacleJoint.rotation = to;

        oc.CallTentaclesAnimation("Spin");
        oc.CallHeadAnimation("Spin");
    }

    private IEnumerator GoUp()
    {
        IsDown = false;
        //yield return new WaitForSeconds(waitTime);
        Quaternion from = tentacleJoint.rotation;
        Quaternion to = tentacleJoint.rotation;
        to *= Quaternion.Euler(rotationAxis * -rotationAngle);

        float elapsed = 0.0f;
        while (elapsed < resetTime)
        {
            tentacleJoint.rotation = Quaternion.Slerp(from, to, elapsed / resetTime);
            elapsed += Time.deltaTime;
            yield return null;
        }
        tentacleJoint.rotation = to;


        IsSlapping = false;
        rotationFlag = true;
        oc.ResetAttack();
    }

    private IEnumerator Slap()
    {
        IsSlapping = true;
        yield return new WaitForSeconds(0.6f);
        rotationFlag = false;
        Quaternion from = tentacleJoint.rotation;
        Quaternion to = tentacleJoint.rotation;
        to *= Quaternion.Euler(rotationAxis * -preAttackAngle);

        float elapsed = 0.0f;
        while (elapsed < preAttackTime)
        {
            tentacleJoint.rotation = Quaternion.Slerp(from, to, elapsed / attackTime);
            elapsed += Time.deltaTime;
            yield return null;
        }
        tentacleJoint.rotation = to;

        oc.CallEyeAnimation("CloseEyes");
        IsDown = true;
        from = tentacleJoint.rotation;
        to = tentacleJoint.rotation;
        to *= Quaternion.Euler(rotationAxis * (rotationAngle + preAttackAngle));

        elapsed = 0.0f;
        while (elapsed < attackTime)
        {
            tentacleJoint.rotation = Quaternion.Slerp(from, to, elapsed / attackTime);
            elapsed += Time.deltaTime;
            yield return null;
        }
        tentacleJoint.rotation = to;
        if(!oc.IsProtected) oc.CallEyeAnimation("OpenEyes");


        yield return new WaitForSeconds(waitTime/4);
        IsDown = false;
        yield return new WaitForSeconds(waitTime*3/4);



        from = tentacleJoint.rotation;
        to = tentacleJoint.rotation;
        to *= Quaternion.Euler(rotationAxis * -rotationAngle);

        elapsed = 0.0f;
        while (elapsed < resetTime)
        {
            tentacleJoint.rotation = Quaternion.Slerp(from, to, elapsed / resetTime);
            elapsed += Time.deltaTime;
            yield return null;
        }
        tentacleJoint.rotation = to;

        oc.ResetAttack();
        IsSlapping = false;
        rotationFlag = true;
    }

    private bool IsInRadius() => Vector3.Distance(player.position, tentacleBase.position) < detectionRadius;

    private bool IsInRadius(float add) => Vector3.Distance(player.position, tentacleBase.position) < detectionRadius + add;
}
