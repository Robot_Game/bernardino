﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleJointController : MonoBehaviour
{
    private TentacleController tc;
    private PlayerController pc;
    private OctoController oc;

    private void Start()
    {
        tc = GetComponentInParent<TentacleController>();
        pc = FindObjectOfType<PlayerController>();
        oc = GetComponentInParent<OctoController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9 && pc.IsAlive)
        {

            if ((tc.IsDown || oc.IsSpinning()) && oc.IsAlive)
            {
                pc.Die();
            }
        }
    }

    /*private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == 9 && pc.IsAlive)
        {

            Debug.Log("Hit Player but not to attack");
            if (oc.IsSpinning())
            {
                Debug.Log("Kill Player crl!!");
                pc.Die();
            }
        }
    }*/

}
