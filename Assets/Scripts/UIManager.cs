﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI deathText;
    [SerializeField] private TextMeshProUGUI respawnText;
    [SerializeField] private TMP_FontAsset[] deathScreenFonts;
    [TextArea(1, 2)]
    [SerializeField] private string[] deathScreenTexts;
    [SerializeField] private GameObject[] grabFeedbackImages = new GameObject[2];

    private TMP_FontAsset currentFont;
    private TMP_FontAsset oldFont;
    private string oldDeathText;
    private string currentDeathText;
    private int deathCounter = 0;

    public void EnableGrabFeedback(int i, bool enable = true)
    {
        if (i == 0)
            grabFeedbackImages[0].SetActive(enable);
        if (i == 1)
            grabFeedbackImages[1].SetActive(enable);

    }

    public void ChangeDeathScreen()
    {
        TMP_FontAsset newFont;

        newFont = GetRandomFont();

        deathText.font = newFont;
        respawnText.font = newFont;

        deathText.text = GetRandomText();

        deathCounter++;
    }

    private TMP_FontAsset GetRandomFont()
    {
        int rand;

        while (currentFont == oldFont)
        {
            rand = Random.Range(0, deathScreenFonts.Length);
            currentFont = deathScreenFonts[rand];
        }
        oldFont = currentFont;
        return currentFont;
    }

    private string GetRandomText()
    {
        if (deathCounter == 0)
        {
            deathCounter++;
            return deathScreenTexts[0];
        }

        int rand;

        while (currentDeathText == oldDeathText)
        {
            rand = Random.Range(0, deathScreenTexts.Length);
            currentDeathText = deathScreenTexts[rand];
        }
        oldDeathText = currentDeathText;
        return currentDeathText;
    }
}
