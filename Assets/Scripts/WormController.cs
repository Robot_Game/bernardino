﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormController : MonoBehaviour
{
    [SerializeField] private float countdownTime;
    [SerializeField] private float difficultyMultiplier;
    [SerializeField] private WormDeathTrigger deathTrigger;
    [SerializeField] private WormsDetectionTriggers detectionTrigger;

    private Animator wormAnimator;

    public bool IsAlive { get; private set; } = true;

    // Start is called before the first frame update
    void Start()
    {
        wormAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (IsAlive) Die();
        }
        if (deathTrigger.Swallowed && IsAlive) Die();
    }

    public void CallCountdown()
    {
        StartCoroutine(Countdown());
    }

    public void StopCountdown()
    {
        StopCoroutine(Countdown());
    }

    private IEnumerator Countdown()
    {
        yield return new WaitForSeconds(countdownTime);

        CallAnimation("Jump");
        detectionTrigger.ResetBool();
    }

    private void CallAnimation(string anim)
    {
        wormAnimator.SetTrigger(anim);
    }

    private void Die()
    {
        IsAlive = false;

        GetComponent<Animator>().SetTrigger("Die");
    }
}
