﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormDeathTrigger : MonoBehaviour
{

    public bool Swallowed { get; private set; } = false;

    private PlayerController pc;


    bool flag = false;

    private void Start()
    {
        pc = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!flag && other.gameObject.name.Contains("piece"))
        {
            Swallowed = true;
        }

        if (other.gameObject.layer == 9 && pc.IsAlive)
        {
            Debug.Log("KILL THE MODAFOKA");
            pc.Die();
        }

    }
}
