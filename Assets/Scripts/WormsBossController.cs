﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormsBossController : MonoBehaviour
{
    [SerializeField] private float spawnDistance = 30;
    [SerializeField] private WormController[] wormController = new WormController[2];
    [SerializeField] private WormsDetectionTriggers[] triggers = new WormsDetectionTriggers[2];
    [SerializeField] private Animator doorAnimator;

    private Animator bossAnimator;

    private bool isSleeping = true;

    private bool[] countdownFlags = new bool[2];

    // Player
    private Transform player;

    public bool IsAlive { get; private set; } = true;


    // Start is called before the first frame update
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player").transform;
        bossAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isSleeping &&
                Vector3.Distance(transform.position, player.position) < spawnDistance) Spawn();

        if (triggers[0].IsDetecting && !countdownFlags[0])
        {
            wormController[0].CallCountdown();
            countdownFlags[0] = true;
        }

        if (!triggers[0].IsDetecting && countdownFlags[0])
        {
            wormController[1].StopCountdown();
            countdownFlags[0] = false;
        }

        if (triggers[1].IsDetecting && !countdownFlags[1])
        {
            wormController[1].CallCountdown();
            countdownFlags[1] = true;
        }

        if (!triggers[1].IsDetecting && countdownFlags[1])
        {
            countdownFlags[1] = false;
            wormController[1].StopCountdown();
        }

        if (!wormController[0].IsAlive && !wormController[1].IsAlive && IsAlive) Die();
    }

    private void Spawn()
    {
        if (isSleeping)
        {
            doorAnimator.GetComponent<Animator>().SetTrigger("Close");
            StartCoroutine(PlaySpawnAnim());
            isSleeping = false;

        }
    }

    private IEnumerator PlaySpawnAnim()
    {
        while (doorAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        GetComponent<Animator>().SetTrigger("Spawn");
    }

    private void Die()
    {
        IsAlive = false;

        StartCoroutine(OpenDoor());
    }

    private IEnumerator OpenDoor()
    {
        yield return new WaitForSeconds(0.5f);

        while (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        doorAnimator.GetComponent<Animator>().SetTrigger("Open");
    }
}
