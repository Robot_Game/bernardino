﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormsDetectionTriggers : MonoBehaviour
{

    public bool IsDetecting { get; private set; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            IsDetecting = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            IsDetecting = false;
        }
    }

    public void ResetBool()
    {
        IsDetecting = false;
    }
}
