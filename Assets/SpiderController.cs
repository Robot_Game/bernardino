﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderController : MonoBehaviour
{

    [SerializeField] private float spawnDistance = 30;
    [SerializeField] private Animator doorAnimator;

    private Animator bossAnimator;

    private BrainController bc;
    private Transform player;
    private List<Rigidbody> spikes;
    private GameObject[] spikesObjects;

    private bool isSleeping = true;

    public bool IsAlive { get; private set; } = true;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        bc = FindObjectOfType<BrainController>();
        bossAnimator = GetComponent<Animator>();
        GetSpikes();
    }

    // Update is called once per frame
    void Update()
    {
        if (isSleeping &&
            Vector3.Distance(transform.position, player.position) < spawnDistance) Spawn();
        if (Input.GetKeyDown(KeyCode.Alpha2)) Die();
    }

    private void GetSpikes()
    {
        spikesObjects = GameObject.FindGameObjectsWithTag("Spike");

        spikes = new List<Rigidbody>();

        foreach(GameObject g in spikesObjects)
        {
            if (g.name == "spike" && g.GetComponent<Rigidbody>() != null) spikes.Add(g.GetComponent<Rigidbody>());
        }
    }

    private void DropSpikes()
    {
        foreach (Rigidbody spike in spikes)
        {
            spike.isKinematic = false;
            spike.transform.parent = null;
        }
    }

    private void Spawn()
    {
        if (isSleeping)
        {
            doorAnimator.GetComponent<Animator>().SetTrigger("Close");
            isSleeping = false;

        }
    }

    public void Die()
    {
        IsAlive = false;

        bc.TransformBrain();

        DropSpikes();

        bossAnimator.SetTrigger("Die");

        StartCoroutine(OpenDoor());
    }

    private IEnumerator OpenDoor()
    {
        yield return new WaitForSeconds(0.5f);

        while (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        doorAnimator.GetComponent<Animator>().SetTrigger("Open");
    }
}
